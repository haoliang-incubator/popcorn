import pytest
from asks import Session
from asks.errors import RequestTimeout


async def test_relpath_with_port():
    client = Session(base_location="http://example.com:80")

    resp = await client.get(path="nowhere")
    assert resp.status_code == 404


@pytest.mark.slow
async def test_relpath_without_port():
    client = Session(base_location="http://example.com:81")

    with pytest.raises(RequestTimeout):
        await client.get(path="/nowhere", connection_timeout=5)


async def test_connect_target_close():
    client = Session(base_location="http://localhost:79")

    try:
        await client.get(path="/nowhere")
    except OSError as e:
        assert e.args == ("All connection attempts failed",)
