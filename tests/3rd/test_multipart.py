import pytest
from multipart import QuerystringParser


# pylint: disable=invalid-name
def test_repr_QuerystringParser():
    parser = QuerystringParser(callbacks={})

    with pytest.raises(AttributeError):
        repr(parser)
