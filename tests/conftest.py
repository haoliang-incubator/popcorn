import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--run-slow", action="store_true", help="whether run slow tests or not"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--run-slow"):
        return

    skip_slow = pytest.mark.skip(reason="requires --run-slow option to run")

    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)
