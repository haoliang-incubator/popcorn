#!/usr/bin/env bash
# shellcheck disable=SC2155,SC2034

set -e

readonly vard=$(dirname $(realpath $0))/../var
readonly base=localhost:8082

# # client form: field & file
# curl -S ${base}/upload/dryrun \
#     -F "f1=@${vard}/8k" \
#     -F 'who=haoliang' \
#     -F 'want=time' \
#     -vv

# # server: chunked
# curl -Si ${base}/tick/3

# # client chunked
# for i in {1..4}; do echo $i; sleep 1; done | \
#     curl -T - ${base}/echo -X POST

# keepalive
# curl -S ${base}/ping ${base}/ping -vv


curl -S ${base}/echo.json \
    -H 'content-type: application/json' \
    -d '{"hello": "world"'
