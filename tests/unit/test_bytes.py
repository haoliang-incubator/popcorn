# pylint: disable=invalid-name

"""
test:
* add, iadd
* copy
* reference
* slice
"""

from functools import partial

import pytest
from memory_profiler import profile


def _filler(bytes: int):
    with open("/dev/urandom", "rb") as fp:
        return fp.read(bytes)


origin = partial(_filler, 5 * 10 ** 6)  # 5M
incr = partial(_filler, 20 * 10 ** 6)  # 20M


@profile
def profile_bytes():
    bs = bytes(origin())  # +4.5M
    bs += incr()  # +19.3M
    bs += b"x"  # +0
    b3 = bs + b"x"  # +23.7M
    b4 = b3[:-1]  # +23.7M
    b5 = memoryview(b3)[:-1]  # +0

    return b4, b5


@profile
def profile_bytearray():
    ba = bytearray(origin())  # +4.8M
    ba += incr()  # +19.1M
    ba += b"x"  # +0
    ba = ba + b"yz"  # +0
    b2 = ba + b"yz"  # +23.7M
    b3 = b2[:-1]  # +23.7M
    b4 = memoryview(b2)[:-1]  # +0

    return b3, b4


def test_memoryview_api():
    mv = memoryview(bytearray(b"abcd"))
    with pytest.raises(TypeError):
        mv + b"x"  # pylint: disable=pointless-statement

    assert mv[1:-1].tobytes() == b"bc"
    mv[1:-1] = b"xy"
    assert mv[1:-1].tobytes() == b"xy"


if __name__ == "__main__":
    profile_bytes()
    # profile_bytearray()
