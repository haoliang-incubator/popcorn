# pylint: disable=invalid-name

import h11
import pytest


def test_type_cast():
    headers = [
        ("server", "unknown/0.0.1"),
        ("content-length", "0"),
    ]

    response = h11.Response(
        status_code=200,
        headers=headers,
        http_version=b"1.1",
        reason=b"OK",
        _parsed=False,
    )

    h: bytes
    for h, v in response.headers:
        assert isinstance(h, bytes)
        assert isinstance(v, bytes)
        assert h.islower()


def test_invalid_header_pair():
    feeds = [
        ([("世界", "你好")], UnicodeEncodeError),
        ([("<x>", "whatever")], h11.LocalProtocolError),
        ([("content-length", 0)], TypeError),
    ]

    for headers, err in feeds:
        with pytest.raises(err):
            h11.Response(
                status_code=200,
                headers=headers,
                http_version=b"1.1",
                reason=b"OK",
                _parsed=False,
            )
