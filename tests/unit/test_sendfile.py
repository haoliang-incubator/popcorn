from datetime import timedelta

import pytest
import trio
from h11 import Request

from avid.httplib import SendFile, httpdate
from avid.ui.web.assets import AssetsFactory
from tests import testdata


async def test_basic_apis(books_json: SendFile):
    file = books_json

    content_type = file.content_type
    assert content_type == b"application/json; charset=utf-8"


async def test_if_modified_since(books_json: SendFile):
    mtime = books_json.info.mtime
    feed = [
        (mtime - timedelta(minutes=1), True),
        (mtime, False),
        (mtime + timedelta(minutes=1), False),
    ]

    for header, expect in feed:
        _mtime = httpdate.as_header(header)
        file = await _make_file([(b"if-modified-since", _mtime)])
        assert (file.info.mtime > header) is expect
        assert file.have_modified_since() is expect


async def test_if_none_match(books_json: SendFile):
    etag = books_json.info.etag
    feed = [
        (b"*", True),
        (b"xxoo,ooxx", True),
        (etag, False),
    ]

    for hv, expect in feed:
        file = await _make_file([(b"if-none-match", hv)])
        assert file.have_no_match() is expect


async def test_modified_and_match_missing():
    file = await _make_file([])

    with pytest.raises(KeyError):
        assert file.have_modified_since()

    with pytest.raises(KeyError):
        assert file.have_no_match()


async def _make_file(headers: list):
    fileinfo = await AssetsFactory._file_info(trio.Path(testdata.books_json))

    headers.append((b"host", b"example.com"))
    request = Request(
        method=b"GET",
        target=b"/nowhere/books.json",
        headers=headers,
        http_version=b"1.1",
    )
    return SendFile(fileinfo, request)


@pytest.fixture(scope="function")
async def books_json():
    return await _make_file([])
