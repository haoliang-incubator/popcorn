import urllib.parse
from os import path

import hyperlink


def test_by_hyperlink():

    url = hyperlink.parse("http://example.com/x/y/")
    assert url.absolute
    assert url.path == ("x", "y", "")

    url = hyperlink.parse("http://example.com/x/y#c/?a=b")
    assert url.absolute
    assert url.path == ("x", "y")

    url = hyperlink.parse("x/y#c?a=b")
    assert not url.absolute
    assert url.path == ("x", "y")

    url = hyperlink.parse("/../x/y#c?a=b")
    assert not url.absolute
    assert url.path == ("..", "x", "y")

    normalized = url.normalize()
    assert not normalized.absolute
    assert normalized.path == ("x", "y")

    url = hyperlink.parse("http://example.com/x/../y#c?a=b").normalize()
    assert url.absolute
    assert url.path == ("y",)


def test_urllib():

    url = urllib.parse.urlparse("http://example.com/x/y/")
    assert url.path == "/x/y/"

    url = urllib.parse.urlparse("http://example.com/x/y#c?a=b")
    assert url.path == "/x/y"

    url = urllib.parse.urlparse("x/y#c?a=b")
    assert url.path == "x/y"

    url = urllib.parse.urlparse("/../x/y#c?a=b")
    assert url.path == "/../x/y"
    assert path.normpath(url.path) == "/x/y"

    url = urllib.parse.urlparse("http://example.com/x/../y#c?a=b")
    assert url.path == "/x/../y"
    assert path.normpath(url.path) == "/y"
