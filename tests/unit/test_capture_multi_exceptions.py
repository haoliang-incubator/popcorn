import pytest


def test_capture_multi_exc():
    excs = (KeyError, ValueError, TypeError)

    feed = [
            KeyError('missing key'),
            ValueError('out of bounds'),
            TypeError('incorrect type'),
            ]

    for e in feed:
        try:
            raise e
        except excs as e:
            print(e)
        else:
            pytest.fail('should fail')
