from pathlib import PosixPath, PurePath


def test_out_of_jail():
    jail = PurePath("/jail/shutter/island")

    facts = [
        ("/", "jail/shutter/island"),
        # ("/freeland", "N/A"), # value error
        # ("/jail/shutter/island/cave/", "N/A"), # value error
    ]

    for relative_to, expect in facts:
        rel = jail.relative_to(PurePath(relative_to))
        assert str(rel) == expect


def test_absoluate():
    feed = [
        ("/usr/share/./..", "/usr"),
        ("/usr/share/./../../..", "/"),
    ]

    for plain, expect in feed:
        path = PosixPath(plain).resolve()
        assert str(path) == expect
