import datetime
import time
from wsgiref.handlers import format_date_time

from avid.httplib import httpdate


def test_second_gap():
    s1 = format_date_time(None)
    s2 = format_date_time(time.time())
    s3 = httpdate.now_as_string()

    assert s1 == s2 == s3


def test_from_string():
    correct_string = format_date_time(None)
    utcdt = httpdate.from_string(correct_string)

    assert httpdate.as_string(utcdt) == correct_string


def test_from_now():
    correct_string = format_date_time(None)
    utcdt = httpdate.from_local_now()

    assert httpdate.as_string(utcdt) == correct_string


def test_from_datetime():
    now = datetime.datetime.now()
    correct_string = format_date_time(now.timestamp())

    utcdt = httpdate.from_local_datetime(now)

    assert httpdate.as_string(utcdt) == correct_string


def test_from_timestamp():
    correct_string = format_date_time(None)

    utcdt = httpdate.from_local_timestamp(time.time())

    assert httpdate.as_string(utcdt) == correct_string
