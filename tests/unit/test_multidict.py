# pylint: disable=import-outside-toplevel

"""
for headers:
* case insensitive
* order preserved: append/push
* normalize: [(b"k:ascii", b"v:ascii")]
* distinct

after all, I think [(b"k", b"v")] is best way to maintain headers with caution
"""

import pytest


def test_multidict_api():
    try:
        from multidict import CIMultiDict
    except ImportError:
        pytest.skip("without multidict installed")
        return

    md = CIMultiDict(a="a", B="B", c="c", d=ord("d"), b="b")

    assert md["a"] == md["A"] == "a"
    assert md["b"] == md["B"] == "B"
    assert md["d"] == md["D"] == ord("d")
    assert md.getall("b") == ["B", "b"]

    assert tuple(md.keys()) == tuple("aBcdb")

    assert md["c"] == md["C"] == "c"
    md["C"] = "C"
    assert md["c"] == md["C"] == "C"
    assert md.getall("c") == ["C"]


def test_bottle_multidict_api():
    from bottle import HeaderDict

    md = HeaderDict(a="a", B="B", c="c", d=ord("d"), b="b")

    assert md["a"] == md["A"] == "a"
    assert md["b"] == md["B"] == "b"  # !
    assert md["d"] == md["D"] == str(ord("d"))  # !
    assert md.getall("b") == ["b"]  # !

    assert tuple(md.keys()) == tuple("ABCD")  # !

    assert md["c"] == md["C"] == "c"
    md["C"] = "C"
    assert md["c"] == md["C"] == "C"
    assert md.getall("c") == ["C"]
