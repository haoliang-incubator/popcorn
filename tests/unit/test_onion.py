# pylint: disable=invalid-name

from functools import wraps

import pytest

from avid.ui.web.onion import Onion


@pytest.fixture()
def alphabet():
    # #{{{
    def pre_a(func):
        @wraps(func)
        def inner():
            return "a" + func()

        return inner

    def pre_b(func):
        @wraps(func)
        def inner():
            return "b" + func()

        return inner

    def pre_c(func):
        @wraps(func)
        def inner():
            return "c" + func()

        return inner

    def post_x(func):
        @wraps(func)
        def inner():
            return func() + "x"

        return inner

    def post_y(func):
        @wraps(func)
        def inner():
            return func() + "y"

        return inner

    def post_z(func):
        @wraps(func)
        def inner():
            return func() + "z"

        return inner

    # #}}}

    return Onion([pre_a, pre_b, pre_c], [post_x, post_y, post_z])


def test_flow():
    o = Onion(("a", "b", "c"), ("x", "y", "z"))

    assert o.flow == tuple("xyzcba")


def test_alphabet(alphabet: Onion):
    wrapped = alphabet.wrap(lambda: "**")
    assert wrapped() == "abc**xyz"


def test_shortcuts(alphabet: Onion):
    @alphabet.deco(excludes=("post_y", "pre_c"))
    def stars():
        return "**"

    assert stars.__name__ == "stars"
    assert stars() == "ab**xz"
