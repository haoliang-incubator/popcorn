import pathlib

import trio
from avid.ui.web.assets import AssetsFactory


async def test_assets_works():
    root = pathlib.Path(__file__).resolve().parent.parent.parent.joinpath("avid")

    at = await AssetsFactory.from_paths(trio.Path(root))

    assert len(at.db) >= 1
