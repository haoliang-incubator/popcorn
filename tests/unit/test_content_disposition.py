# pylint: disable=invalid-name

import email._header_value_parser as emailparser

import pytest
from avid.httplib.formparser import ContentDisposition


def test_parsing_content_disposition_in_form_data():
    value = 'form-data; name="file\\"; filename"; filename="8k"'
    parsed = emailparser.parse_content_disposition_header(value)

    assert parsed


parsed_result = """
ContentDisposition([
    Token([ValueTerminal('form-data')]),
    ValueTerminal(';'),
    MimeParameters([
        Parameter([
            Attribute([
                CFWSList([WhiteSpaceTerminal(' ')]),
                ValueTerminal('name')
            ]),
            ValueTerminal('='),
            Value([
                QuotedString([BareQuotedString([ValueTerminal('file";'),
                WhiteSpaceTerminal(' '), ValueTerminal('filename')])])
            ])
        ]),
        ValueTerminal(';'),
        Parameter([
            Attribute([
                CFWSList([WhiteSpaceTerminal(' ')]),
                ValueTerminal('filename')
            ]),
            ValueTerminal('='),
            Value([QuotedString([BareQuotedString([ValueTerminal('8k')])])])
        ])
    ])
])
"""


def test_parsing_content_disposition_manually():
    feed = [
        (
            b'form-data; name="file\\"; filename"; filename="8k"',
            [(b"name", b'file"; filename'), (b"filename", b"8k")],
        ),
        (
            b'form-data; name="file\\"; filename\\" anothername"; filename="8k"',
            [(b"name", b'file"; filename" anothername'), (b"filename", b"8k")],
        ),
        (
            b'form-data; name="file\\"; filename\\" anothername\\"\\""; filename="8k"',
            [(b"name", b'file"; filename" anothername""'), (b"filename", b"8k")],
        ),
        (
            b'form-data; filename="filename with ; semicolon.txt"',
            [(b"filename", b"filename with ; semicolon.txt")],
        ),
        (
            b'form-data; name="fieldx"; filename="8k"',
            [(b"name", b"fieldx"), (b"filename", b"8k")],
        ),
    ]

    for value, expect in feed:
        parsed = ContentDisposition.parse(value)

        assert list(parsed) == expect


def test_parsing_content_disposition_fails():

    with pytest.raises(AssertionError):
        # 中文.txt
        list(
            ContentDisposition.parse(
                b"form-data; name=\"name\"; filename*=UTF-8''%E4%B8%AD%E6%96%87.txt"
            )
        )

    parsed = list(
        ContentDisposition.parse(b'form-data; name="foo"; filename="foo.txt"; size=123')
    )
    assert parsed != [(b"name", b"foo"), (b"filename", b"foo.txt"), (b"size", b"123")]


def test_ContentDisposition():
    parser = ContentDisposition.from_raw(b'form-data; name="fieldx"; filename="8k"')

    assert parser[b"name"] == b"fieldx"
    assert parser[b"filename"] == b"8k"
    assert b"size" not in parser


def test_escaped_string():
    assert r"\"".encode() == b'\\"'
    assert '\\"'.encode() == b'\\"'
    assert '"\\""'.encode() == r'"\""'.encode()
