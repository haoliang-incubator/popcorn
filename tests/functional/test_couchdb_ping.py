import pytest
from asks import Session
from asks.auth import BasicAuth
from asks.response_objects import Response

from . import facts


@pytest.fixture(scope="module")
def client():
    return Session(base_location=facts.couchdb_addr)


@pytest.fixture(scope="module")
def auth():
    return BasicAuth(facts.couchdb_auth)


async def test_ping(client: Session):
    resp: Response = await client.get(path="/")

    assert resp.status_code == 200
    assert resp.headers["Content-Type"] == "application/json"
    payload = resp.json()
    assert "couchdb" in payload
    assert payload["couchdb"] == "Welcome"


async def test_get_uuid(client: Session, auth):
    resp: Response = await client.get(path="/_uuids", auth=auth)

    assert resp.status_code == 200
    assert resp.headers["Content-Type"] == "application/json"
    payload = resp.json()
    assert "uuids" in payload
