# pylint: disable=invalid-name

import pytest
from asks import Session
from asks.auth import BasicAuth
from asks.response_objects import Response

from . import facts


@pytest.fixture(scope="module")
def client():
    return Session(base_location=facts.avid_addr)


async def test_ping(client):
    resp: Response = await client.get(path="/ping")
    assert resp.body == b"pong"


async def test_ping_HEAD(client):
    resp: Response = await client.head(path="/ping")
    assert "content-length" in resp.headers
    assert int(resp.headers["content-length"]) > 0
    assert resp.body == b""


async def test_count(client: Session):
    reset_rp: Response = await client.delete(path="/count")
    assert reset_rp.body == b"0"

    next_rp: Response = await client.get(path="/count")
    assert next_rp.body == b"1"


async def test_echo(client):
    resp: Response = await client.get(path="/echo")
    payload = resp.json()

    assert "method" in payload
    assert "target" in payload
    assert "headers" in payload
    assert "body" in payload
    assert payload["method"] == "GET"


async def test_stream(client):
    resp: Response = await client.get(path="/tick/2")
    assert "transfer-encoding" in resp.headers
    assert resp.headers["transfer-encoding"] == "chunked"
    body: bytes = resp.body
    lines = body.strip().splitlines(keepends=False)
    assert len(lines) == 2


async def test_stream_HEAD(client):
    resp: Response = await client.head(path="/tick/2")
    assert "transfer-encoding" in resp.headers
    assert resp.headers["transfer-encoding"] == "chunked"
    assert resp.body == b""


async def test_static_file(client):
    resp: Response = await client.get(path="/data")
    assert resp.headers["content-type"].startswith("application/octet-stream")
    assert "content-length" in resp.headers
    assert "etag" in resp.headers
    assert "last-modified" in resp.headers
    assert "cache-control" in resp.headers
    assert "max-age" in resp.headers["cache-control"]
    assert "accept-ranges" in resp.headers
    assert resp.headers["accept-ranges"] == "none"
    assert len(resp.body) > 0


async def test_static_file_HEAD(client):
    resp: Response = await client.head(path="/data")
    assert resp.headers["content-type"].startswith("application/octet-stream")
    assert "content-length" in resp.headers
    assert "etag" in resp.headers
    assert "last-modified" in resp.headers
    assert "cache-control" in resp.headers
    assert "max-age" in resp.headers["cache-control"]
    assert "accept-ranges" in resp.headers
    assert resp.headers["accept-ranges"] == "none"
    assert len(resp.body) == 0


async def test_basic_auth(client: Session):
    resp: Response = await client.get(path="/now", auth=BasicAuth(("frodo", "friend")))
    assert resp.status_code == 200


async def test_cors_preflight(client: Session):
    options_rp: Response = await client.options(path="/cors")

    assert "access-control-allow-origin" in options_rp.headers
    assert "access-control-allow-methods" in options_rp.headers
    assert "access-control-allow-headers" in options_rp.headers
    assert "access-control-max-age" in options_rp.headers
    assert options_rp.status_code == 204


async def test_cors(client: Session):
    resp: Response = await client.get(path="/cors")
    assert "access-control-allow-origin" in resp.headers
    assert resp.status_code == 200


async def test_routing(client: Session):
    feed = [
        (client.get, "/av/1", "GET"),
        (client.put, "/av/1/favorite", "PUT"),
        (client.delete, "/av/1/favorite", "DELETE"),
        (client.put, "/av/1/more", "PUT"),
        (client.delete, "/av/1/more", "DELETE"),
    ]

    for fetcher, path, method in feed:
        resp: Response = await fetcher(path=path)

        assert resp.status_code == 200

        payload = resp.json()

        assert "target" in payload
        assert payload["target"] == path

        assert "method" in payload
        assert payload["method"] == method
