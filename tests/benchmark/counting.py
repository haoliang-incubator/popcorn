import itertools
import math
import pathlib

import asks
import trio

counter = itertools.count()


class Filesystem:
    root = pathlib.Path(__file__).resolve().parent
    countfile = root.joinpath("var", "counts")


async def get_count_asks_nginx(
    no: int, repeat: int, sender: trio.abc.SendChannel, session: asks.Session
):
    async with sender:
        for _ in range(repeat):
            await session.get("http://127.0.0.1")
            await sender.send((no, next(counter)))


async def get_count_asks(
    no: int, repeat: int, sender: trio.abc.SendChannel, session: asks.Session
):
    async with sender:
        for _ in range(repeat):
            rp = await session.get("http://127.0.0.1:8080/count")
            await sender.send((no, int(rp.content)))


async def get_count_locally(no: int, repeat: int, sender: trio.abc.SendChannel):
    async with sender:
        for _ in range(repeat):
            await sender.send((no, next(counter)))


async def save(receiver: trio.abc.ReceiveChannel):
    fp: trio._file_io.AsyncIOWrapper
    async with await trio.open_file(Filesystem.countfile, mode="w") as fp:
        async with receiver:
            async for no, count in receiver:
                await fp.write(f"{no} {count}\n")


async def discard(receiver: trio.abc.ReceiveChannel):
    async with receiver:
        async for _ in receiver:
            pass


async def main():
    pop = 10
    repeat = 5000

    session = asks.Session("http://127.0.0.1", connections=10)
    nursery: trio.Nursery
    async with trio.open_nursery() as nursery:
        sender: trio.abc.SendChannel
        receiver: trio.abc.ReceiveChannel
        sender, receiver = trio.open_memory_channel(math.inf)

        async with sender, receiver:
            for no in range(pop):
                nursery.start_soon(get_count_asks, no, repeat, sender.clone(), session)

            nursery.start_soon(save, receiver.clone())


if __name__ == "__main__":
    trio.run(main)
