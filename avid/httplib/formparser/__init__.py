"""
specs:
* form-urlencoded conflicts with form-data
* query-params can be combined with form-data or form-urlencoded

design:
* do not support duplicate keys in form-data, x-www-form-urlencoded
    * only takes first (or last?) in stream
"""

from . import _illuminating_parsers as illuminating
from .containers import Form, Headers, UploadFile
from .parsers import ContentDisposition, MultipartParser, QuerystringParser
