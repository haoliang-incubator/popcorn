"""
todo: parser unified data container access entry?
"""

import logging
from collections import UserDict
from typing import Callable, Iterable, List, Optional, Tuple

import h11
import multipart
import trio
from avid.httplib import RequestBody

from .containers import Form, Headers, UploadFile

_log = logging.getLogger(__name__)


class QuerystringParser:
    CONTENT_TYPE = b"application/x-www-form-urlencoded"

    def __init__(self, headers: Headers, source: RequestBody):
        self._headers = headers
        self._source = source
        self._parsed = False
        self._pairs: List[Tuple[bytes, bytes]] = []
        self._cursor: int = 0

    @property
    def pairs(self):
        assert self._parsed

        return Form(self._pairs)

    def on_field_start(self):
        try:
            self._pairs[self._cursor]
        except IndexError:
            self._pairs.append([])
        else:
            raise RuntimeError("expects cursor not exists in pairs on_field_start")

    def on_field_name(self, data: bytes, start: int, end: int):
        self._pairs[self._cursor].append(data[start:end])  # type: ignore

    def on_field_data(self, data: bytes, start: int, end: int):
        self._pairs[self._cursor].append(data[start:end])  # type: ignore

    def on_field_end(self):
        self._cursor += 1

    def on_end(self):
        self._parsed = True

    async def parse(self):
        assert not self._parsed

        callbacks = {
            "on_field_start": self.on_field_start,
            "on_field_name": self.on_field_name,
            "on_field_data": self.on_field_data,
            "on_field_end": self.on_field_end,
            "on_end": self.on_end,
        }

        parser = multipart.QuerystringParser(
            callbacks, max_size=self._headers.content_length
        )
        # TODO@haoliang remove-pending: prevent error
        parser.keep_blank_values = True

        async for chunk in self._source:
            parser.write(chunk.data)
        parser.finalize()

    def __repr__(self):
        return "<{}; parsed={!r}, cursor={!r}>".format(
            self.__class__.__name__, self._parsed, self._cursor
        )


class MultipartParser:
    # pylint: disable=no-self-use

    """
    content-disposition: form-data; name=x
        [content-type: application/octet-stream]
        data
    """

    CONTENT_TYPE = b"multipart/form-data"

    def __init__(self, headers: Headers, source: RequestBody, uploaddir: trio.Path):
        self._headers = headers
        self._source = source
        self._uploaddir = uploaddir
        self._parsed = False

        # form1: [<dict[bytes, bytes]:ContentDisposition>, <bytes:content-type>, <memoryview:chunk>, ... ]
        # form2: (<dict[bytes, bytes]:ContentDisposition>, <memoryview:chunk>)
        self._pairs: list = []

        self._cursor = 0
        self._header_handler: Optional[Callable] = None
        self._data_handler: Optional[Callable] = None

    @property
    def pairs(self):
        return Form(self._pairs)

    def _header_content_disposition(self, value: memoryview):
        _value = value.tobytes()

        assert self._pairs[self._cursor] == []
        assert _value.startswith(b"form-data; ")

        cd = ContentDisposition.from_raw(_value)

        self._pairs[self._cursor].append(cd)

        assert self._data_handler is None

        if b"filename" in cd:
            # TODO@haoliang support `filename*`
            self._data_handler = self._data_file_chunk
        else:
            self._data_handler = self._data_field_value

    def _header_content_type(self, value: memoryview):
        assert self._pairs[self._cursor]

        ctype = value.tobytes()
        self._pairs[self._cursor].append(ctype)

        # pylint: disable=comparison-with-callable
        assert self._data_handler == self._data_file_chunk

    def on_part_begin(self):
        try:
            self._pairs[self._cursor]
        except IndexError:
            self._pairs.append([])
        else:
            raise RuntimeError("expects cursor not exists in pairs on_part_begin")

        assert self._data_handler is None

    def _data_file_chunk(self, value: memoryview):
        """
        due to multipart not support async parse, writing to physical file will blocking whole system
        so we just store all files into memory for now
        """
        self._pairs[self._cursor].append(value)

    def _data_field_value(self, value: memoryview):
        self._pairs[self._cursor].append(value.tobytes())

    def on_part_data(self, data: memoryview, start: int, end: int):
        assert self._data_handler is not None

        value = data[start:end]
        if not isinstance(value, memoryview):
            value = memoryview(value)

        self._data_handler(value)

    def on_part_end(self):
        self._cursor += 1
        self._data_handler = None

    def on_header_field(self, data: memoryview, start: int, end: int):
        header = data[start:end]
        if header == b"Content-Disposition":
            self._header_handler = self._header_content_disposition
        elif header == b"Content-Type":
            self._header_handler = self._header_content_type
        else:
            raise RuntimeError("unknown header of multipart/form-data body")

    def on_header_value(self, data: memoryview, start: int, end: int):
        assert self._header_handler is not None
        self._header_handler(data[start:end])

    def on_header_end(self):
        _log.debug("on_header_end")

    def on_headers_finished(self):
        _log.debug("on_headers_finished")

    def on_end(self):
        self._parsed = True

    async def parse(self):
        if self._parsed:
            raise RuntimeError("has parsed")

        callbacks = {
            "on_part_begin": self.on_part_begin,
            "on_part_data": self.on_part_data,
            "on_part_end": self.on_part_end,
            "on_header_field": self.on_header_field,
            "on_header_value": self.on_header_value,
            "on_header_end": self.on_header_end,
            "on_headers_finished": self.on_headers_finished,
            "on_end": self.on_end,
        }

        parser = multipart.MultipartParser(
            self._headers.boundary, callbacks, max_size=self._headers.content_length
        )

        chunk: h11.Data
        async for chunk in self._source:
            data = memoryview(chunk.data)
            parser.write(data)
            await self._files_to_disk(slice(None, -1))

        parser.finalize()
        await self._files_to_disk(slice(-1, None))

    async def _files_to_disk(self, parts):
        cd_at = 0
        ctype_at = 1
        upfile_at = 2

        for line in self._pairs[parts]:
            # form: a=b
            if len(line) <= 2:
                continue

            # file has been writen to disk
            if isinstance(line[upfile_at], UploadFile):
                continue

            upfile = UploadFile(self._uploaddir, line[cd_at].filename, line[ctype_at])
            async with await upfile.open_for_writing() as fp:
                while len(line) > 2:
                    # release chunks's reference
                    chunk = line.pop(2)
                    await fp.write(chunk)

            line.append(upfile)


class ContentDisposition(UserDict):
    """
    type: Dict[bytes, bytes]
    """

    def filename(self) -> str:
        """
        :raise: KeyError
        """

        return self[b"filename"].decode()

    @classmethod
    def from_raw(cls, raw: bytes):
        return cls(dict(cls.parse(raw)))

    @staticmethod
    def parse(raw: bytes) -> Iterable[Tuple[bytes, bytes]]:
        """
        limits:
        * only support form-data; no `attachment`
        * not support disp-ext-parm yet, eg `filename*=UTF-8''%E4%B8%AD%E6%96%87..txt`
        * not support unquoted value, eg `size=123`

        NB: '"\\""' == r'"\""'; len('\\"') == len(r'\"')

        see:
        * grammar: https://tools.ietf.org/html/rfc6266#section-4.1
        * value definition: https://tools.ietf.org/html/rfc2616#section-2.2
            * especially for double quote and backward slash

        :raise: ParsingError, AssertionError
        """
        assert b"filename*=" not in raw, "filename* is not supported"

        prefix = b"form-data;"
        assert raw.startswith(prefix), "only support form-data"

        field_start = len(prefix)
        while True:
            field_end = raw.find(b'="', field_start)
            if field_end == -1:
                break

            value_start = field_end + 2  # `="`
            _valcursor = value_start
            for _ in range(len(raw[value_start:])):
                _valend = raw.find(b'"', _valcursor)
                if _valend == -1:
                    raise ValueError("can not find value end")
                if raw[_valend - 1 : _valend + 1] != b'\\"':
                    value_end = _valend
                    break
                _valcursor = _valend + 1  # `"`
            else:
                raise ValueError("can not find value end")

            field_slice = slice(field_start, field_end)
            value_slice = slice(value_start, value_end)

            yield (
                raw[field_slice].strip(),
                raw[value_slice].decode("unicode_escape").encode(),
            )

            if raw[value_end:] == b'"':
                break

            assert raw[value_end : value_end + 3] == b'"; '
            field_start = value_end + 3
