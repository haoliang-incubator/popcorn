import uuid
from collections import UserList
from typing import Iterable, List, Tuple

import trio
from avid.httplib.statuses import BadRequest
from cached_property import cached_property


class Headers:
    def __init__(self, headers: List[Tuple[bytes, bytes]]):
        self._headers = headers

    @cached_property
    def content_length(self) -> int:
        clen = self._find_or_err(b"content-length")

        return int(clen)

    @cached_property
    def content_type(self) -> bytes:
        return self._find_or_err(b"content-type")

    @cached_property
    def boundary(self):
        prefix = b"multipart/form-data; boundary="
        ctype = self.content_type
        assert ctype.startswith(prefix)
        return ctype[len(prefix) :]

    def _find_or_err(self, header: bytes) -> bytes:
        for hk, hv in self._headers:
            if hk == header:
                return hv
        raise BadRequest("requires {} header".format(header.decode("ascii")))


class UploadFile:
    """
    contexts:
        with have UploadFile as file:
            with file.open() as fp:
                fp.write()
            # fp closed
        # file adopted or discarded
    """

    def __init__(self, tempdir: trio.Path, filename: bytes, ctype: bytes):
        self._file = tempdir.joinpath(uuid.uuid4().hex)

        self.filename = filename
        self.ctype = ctype

        self._created = False
        self._adopted = False

    async def open_for_writing(self):
        fp = await self._file.open("wb")
        self._created = True
        await self._file.chmod(0o0600)
        return fp

    async def __aenter__(self):
        return self

    async def _unlink(self):
        if not self._created:
            return
        if self._adopted:
            return
        await self._file.unlink()

    async def __aexit__(self, exctype, excval, traceback):
        if excval is not None:
            await self._unlink()
            return

        if not self._adopted:
            await self._unlink()

    async def mv(self, to: trio.Path):  # pylint: disable=invalid-name
        await self._file.replace(to)
        self._adopted = True


class Form(UserList):
    """
    form 1
    * List[Tuple(ContentDisposition, content-type, UploadFile)]
    form 2
    * List[Tuple(ContentDisposition, bytes)]

    uses:
        async with contextlib.AsyncExitStack() as stack:
            for file in form.files:
                stack.enter_context(file)

            ... more logic
    """

    def files(self) -> Iterable[UploadFile]:
        for line in self:
            try:
                upfile = line[2]
            except IndexError:
                continue

            if isinstance(upfile, UploadFile):
                yield upfile
