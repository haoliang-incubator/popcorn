import logging
from functools import wraps

import h11
import multipart
from avid.httplib import RequestBody

from .containers import Headers

_log = logging.getLogger(__name__)


def _illuminate(func):
    name = func.__name__

    @wraps(func)
    def handler(self, *args):
        """
        forms:
        * ()
        * (data, start, end)
        """
        klass = self.__class__.__name__

        if args:
            data: memoryview = args[0][args[1] : args[2]]
            mark = ""
            if not isinstance(data, memoryview):
                data = memoryview(data)
                mark = "[!!!]"

            if data.nbytes < 128:
                _log.debug(
                    "%s#%d.%s: %s %s", klass, self._counter, name, mark, data.tobytes()
                )
            else:
                _log.debug(
                    "%s#%d.%s: %s %s ...",
                    klass,
                    self._counter,
                    name,
                    mark,
                    data[:128].tobytes(),
                )
        else:
            _log.debug("%s#%d.%s", klass, self._counter, name)

        self._counter += 1

    return handler


class QuerystringParser:
    # pylint: disable=no-self-use,unused-argument
    CONTENT_TYPE = b"application/x-www-form-urlencoded"

    def __init__(self, headers: Headers, source: RequestBody):
        self._headers = headers
        self._source = source
        self._counter = 0

        self.pairs = ()

    @_illuminate
    def on_field_start(self):
        ...

    @_illuminate
    def on_field_name(self, data: memoryview, start: int, end: int):
        ...

    @_illuminate
    def on_field_data(self, data: memoryview, start: int, end: int):
        ...

    @_illuminate
    def on_field_end(self):
        ...

    @_illuminate
    def on_end(self):
        ...

    async def parse(self):
        callbacks = {
            "on_field_start": self.on_field_start,
            "on_field_name": self.on_field_name,
            "on_field_data": self.on_field_data,
            "on_field_end": self.on_field_end,
            "on_end": self.on_end,
        }

        parser = multipart.QuerystringParser(
            callbacks, max_size=self._headers.content_length
        )
        # FIXME@haoliang remove-pending: prevent error
        parser.keep_blank_values = True

        async for chunk in self._source:
            data = memoryview(chunk.data)
            parser.write(data)

    def __repr__(self):
        return "<{}>".format(self.__class__.__name__)


class MultipartParser:
    # pylint: disable=unused-argument,no-self-use

    """
    content-disposition: form-data; name=x
        [content-type: application/octet-stream]
        data
    """

    CONTENT_TYPE = b"multipart/form-data"

    def __init__(self, headers: Headers, source: RequestBody):
        self._headers = headers
        self._source = source
        self._counter = 0

        self.pairs = ()

    @_illuminate
    def on_part_begin(self):
        ...

    @_illuminate
    def on_part_data(self, data: memoryview, start: int, end: int):
        ...

    @_illuminate
    def on_part_end(self):
        ...

    @_illuminate
    def on_header_field(self, data: memoryview, start: int, end: int):
        ...

    @_illuminate
    def on_header_value(self, data: memoryview, start: int, end: int):
        ...

    @_illuminate
    def on_header_end(self):
        ...

    @_illuminate
    def on_headers_finished(self):
        ...

    @_illuminate
    def on_end(self):
        ...

    async def parse(self):
        callbacks = {
            "on_part_begin": self.on_part_begin,
            "on_part_data": self.on_part_data,
            "on_part_end": self.on_part_end,
            "on_header_field": self.on_header_field,
            "on_header_value": self.on_header_value,
            "on_header_end": self.on_header_end,
            "on_headers_finished": self.on_headers_finished,
            "on_end": self.on_end,
        }

        parser = multipart.MultipartParser(
            self._headers.boundary, callbacks, max_size=self._headers.content_length
        )

        chunk: h11.Data
        async for chunk in self._source:
            data = memoryview(chunk.data)
            parser.write(data)
