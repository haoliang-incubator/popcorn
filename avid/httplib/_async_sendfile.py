import errno
import os

import trio.lowlevel

from .sendfile import FileInfo


async def _send_some(infd: int, outfd: int, offset: int, count: int) -> int:
    """
    :return: sent how many bytes
    :raise: todo
    """

    # we assume the in_fd will be ready easily than out_fd
    await trio.lowlevel.wait_readable(infd)
    await trio.lowlevel.wait_writable(outfd)

    try:
        return os.sendfile(outfd, infd, offset, count)
    except OSError as e:
        if e.errno == errno.EAGAIN:
            raise NotImplementedError("should never meet EAGAIN|WOULDBLOCK") from e

        raise e


async def sendfile(finfo: FileInfo, outfd: int, offset: int, count: int):
    """
    wrapper on os.sendfile with slightly different signature

    :param out_fd: socket fd that writes file to
    """

    assert not os.get_blocking(outfd), "out fd should be non-blocking"

    per_chunk_max = 1024  # in bytes

    async with finfo.file.open("rb") as infp:
        infd = infp.fileno()
        os.set_blocking(infd, False)

        cursor = offset
        remain = finfo.size - (offset + count)

        while remain > 0:

            count = min(per_chunk_max, remain)

            # TODO@haoliang handle possible errors
            sent = await _send_some(infd, outfd, cursor, count)
            await trio.sleep(0)

            # reached eof
            if sent == 0:
                assert remain <= 0, "insane offset, count of infile"
                break

            cursor += sent
            remain -= sent
