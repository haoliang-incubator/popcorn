import logging
import pathlib
from itertools import count
from socket import socket as stdsocket
from traceback import TracebackException
from typing import AsyncIterable, Callable, List, Tuple

import h11
import trio

from . import httpdate
from .sendfile import FilePlaceholder, SendFile
from .typing import RequestBody, bytesify


def basic_response_headers() -> List[Tuple[bytes, bytes]]:
    """
    HTTP requires these headers in all responses (client would do something different here)
    """

    return [
        (b"date", httpdate.now_as_header()),
        # hardcoded
        (b"server", bytesify("avid-http-server/0.0.1")),
    ]


class Logger:
    _LOG = logging.getLogger(__name__)
    _ID = count()

    def __init__(self):
        self.ident = next(self._ID)

    def debug(self, msg, *args):
        self._LOG.debug(f"#%s {msg}", self.ident, *args)

    def info(self, msg, *args):
        self._LOG.info(f"#%s {msg}", self.ident, *args)

    def exception(self, exc):
        traceback = TracebackException.from_exception(
            exc, lookup_lines=True, capture_locals=True
        )
        frames = traceback.format()

        for frame in frames:
            self._LOG.error(frame)


# The core of this could be factored out to be usable for trio-based clients
# too, as well as servers. But as a simplified pedagogical example we don't
# attempt this here.
class Communicator:
    """
    I/O adapter: h11 <-> trio
    """

    # hardcoded
    # in bytes
    READBUFF_SIZE = 4 << 10

    def __init__(self, stream: trio.SocketStream, logger: Logger):
        self.stream = stream
        self.conn = h11.Connection(h11.SERVER)

        # A unique id for this connection, to include in debugging output
        # (useful for understanding what's going on if there are multiple
        # simultaneous clients).
        self.logger = logger

    async def send(self, event):
        # The code below doesn't send ConnectionClosed, so we don't bother
        # handling it here either -- it would require that we do something
        # appropriate when 'data' is None.
        assert not isinstance(event, h11.ConnectionClosed)

        data = self.conn.send(event)
        await self.stream.send_all(data)

    async def _read_from_peer(self, bufsize=None):
        if self.conn.they_are_waiting_for_100_continue:
            self.logger.info("Sending 100 Continue")
            go_ahead = h11.InformationalResponse(
                status_code=100, headers=basic_response_headers()
            )
            await self.send(go_ahead)

        bufsize = bufsize or self.READBUFF_SIZE

        try:
            data = await self.stream.receive_some(bufsize)
            # TODO@haoliang incompleted connection always yield b''
        except ConnectionError as e:
            self.logger.exception(e)
            raise

        if data == b"":
            raise trio.BrokenResourceError()

        self.conn.receive_data(data)

    async def next_event(self, bufsize=None):
        """
        possible events: h11.{Request,Data,EndOfMessage} ?
        """

        while True:
            event = self.conn.next_event()
            if event is h11.NEED_DATA:
                await self._read_from_peer(bufsize)
                continue
            return event

    async def read_empty_request_body(self) -> RequestBody:
        conn = self.conn
        logger = self.logger

        logger.info("receiving request body")
        assert conn.states == {
            h11.CLIENT: h11.SEND_BODY,
            h11.SERVER: h11.SEND_RESPONSE,
        }

        # hardcoded
        readbuff_size = len(b"\r\n\r\n") * 2

        while True:
            event = await self.next_event(readbuff_size)
            logger.info("received event %s", type(event))
            logger.debug("client state: %s", conn.their_state)

            if not isinstance(event, h11.EndOfMessage):
                raise h11.RemoteProtocolError("request body should be empty", 400)

            assert conn.their_state in (h11.DONE, h11.MUST_CLOSE)

            yield h11.Data(data=b"")
            break

    async def read_request_body(self) -> RequestBody:
        conn = self.conn
        logger = self.logger

        logger.info("receiving request body")
        assert conn.states == {
            h11.CLIENT: h11.SEND_BODY,
            h11.SERVER: h11.SEND_RESPONSE,
        }

        # hardcoded
        # TODO@haoliang uses proper bufsize based on request.headers.content-length
        readbuff_size = self.READBUFF_SIZE * 3
        max_body_size = 8 << 20

        traffic = 0

        while True:
            if traffic > max_body_size:
                raise h11.RemoteProtocolError(b"Request Body Too Large", 413)

            event = await self.next_event(readbuff_size)
            logger.info("received event %s", type(event))
            logger.debug("client state: %s", conn.their_state)

            if isinstance(event, h11.EndOfMessage):
                assert conn.their_state in (h11.DONE, h11.MUST_CLOSE)
                break

            assert isinstance(event, h11.Data)
            yield event

            traffic += len(event.data)

    async def readout_request_body(self, request_body: RequestBody, max: int) -> bytes:
        data: h11.Data
        buff = bytearray()
        async for data in request_body:
            if len(buff) > max:
                raise h11.RemoteProtocolError(b"Request Body Too Large", 413)

            buff.extend(data.data)
        return buff

    async def shutdown(self):
        self.logger.info("going to shutdown connection")

        # hardcoded
        timeout = 10

        # When this method is called, it's because we definitely want to kill
        # this connection, either as a clean shutdown or because of some kind
        # of error or loss-of-sync bug, and we no longer care if that violates
        # the protocol or not. So we ignore the state of self.conn, and just
        # go ahead and do the shutdown on the socket directly. (If you're
        # implementing a client you might prefer to send ConnectionClosed()
        # and let it raise an exception if that violates the protocol.)
        #
        try:
            await self.stream.send_eof()
        except trio.BrokenResourceError:
            # They're already gone, nothing to do
            return

        # Wait and read for a bit to give them a chance to see that we closed
        # things, but eventually give up and just close the socket.
        # XX FIXME: possibly we should set SO_LINGER to 0 here, so
        # that in the case where the client has ignored our shutdown and
        # declined to initiate the close themselves, we do a violent shutdown
        # (RST) and avoid the TIME_WAIT?
        # it looks like nginx never does this for keepalive timeouts, and only
        # does it for regular timeouts (slow clients I guess?) if explicitly
        # enabled ("Default: reset_timedout_connection off")
        with trio.move_on_after(timeout):
            try:
                while True:
                    # Attempt to read until EOF
                    got = await self.stream.receive_some(self.READBUFF_SIZE)
                    if not got:
                        break
            except trio.BrokenResourceError:
                pass
            finally:
                await self.stream.aclose()

    async def force_shutdown(self):
        self.logger.info("going to shutdown connection forcefully")

        await trio.aclose_forcefully(self.stream)

    async def send_response(self, resp: h11.Response, body: bytes, headonly: bool):
        assert self.conn.our_state in (h11.IDLE, h11.SEND_RESPONSE)

        self.logger.info(
            "Sending %d response with %d bytes", resp.status_code, len(body)
        )

        # TODO@haoliang ensure content-length in resp.headers

        for h in basic_response_headers():  # pylint: disable=invalid-name
            resp.headers.insert(0, h)

        await self.send(resp)
        assert self.conn.our_state == h11.SEND_BODY

        if not headonly:
            await self.send(h11.Data(data=body))

        await self.send(h11.EndOfMessage())
        assert self.conn.our_state in (h11.DONE, h11.MUST_CLOSE)

    async def send_stream_response(
        self, resp: h11.Response, body: AsyncIterable[bytearray], headonly: bool
    ):
        """
        ABNF:
            chunked-body   = *chunk
                             last-chunk
                             trailer-part
                             CRLF

            chunk          = chunk-size [ chunk-ext ] CRLF
                             chunk-data CRLF
            chunk-size     = 1*HEXDIG
            last-chunk     = 1*("0") [ chunk-ext ] CRLF

            chunk-data     = 1*OCTET ; a sequence of chunk-size octets

        see: https://tools.ietf.org/html/rfc7230#section-4.1
        """

        assert self.conn.our_state in (h11.IDLE, h11.SEND_RESPONSE)

        self.logger.info("Sending %d stream-response", resp.status_code)

        # TODO@haoliang ensure transfer-encodeing:chunked in resp.headers

        for h in basic_response_headers():  # pylint: disable=invalid-name
            resp.headers.insert(0, h)

        await self.send(resp)
        assert self.conn.our_state == h11.SEND_BODY

        if not headonly:
            async for chunk in body:
                # ABNF: chunk = chunk-data CRLF
                assert chunk.endswith(b"\r\n")
                await self.send(h11.Data(data=chunk))

        await self.send(h11.Data(data=b""))
        await self.send(h11.EndOfMessage())
        assert self.conn.our_state in (h11.DONE, h11.MUST_CLOSE)

    async def maybe_send_error_response(self, exc):
        self.logger.info("Trying to send error response...")

        # If we can't send an error, oh well, nothing to be done
        if self.conn.our_state not in (h11.IDLE, h11.SEND_RESPONSE):
            self.logger.info(
                "...but I can't, because our state is %s", self.conn.our_state
            )
            return

        if isinstance(exc, h11.ProtocolError):
            status_code = exc.error_status_hint
        elif isinstance(exc, trio.TooSlowError):
            status_code = 408  # Request Timeout
        else:
            status_code = 500

        body = str(exc).encode("utf-8")
        headers = basic_response_headers()
        headers.append((b"content-type", bytesify("text/plain; charset=utf-8")))
        headers.append((b"content-length", bytesify(str(len(body)))))
        resp = h11.Response(status_code=status_code, headers=headers)

        # TODO@haoliang cannt response body with a HEAD request
        await self.send_response(resp, body, headonly=False)

    async def send_file_response(
        self, resp: h11.Response, file: SendFile, headonly: bool
    ):
        """
        it will take care of resp.header.{content-type, content-length},
        but you should check whether the file exists or not by yourself!
        """
        assert self.conn.our_state in (h11.IDLE, h11.SEND_RESPONSE,)
        self.logger.info("Sending %d response with file %s", resp.status_code, file)

        async def _response_304(resp: h11.Response):
            resp.status_code = 304
            resp.reason = b"Not Modified"
            await self.send_response(resp, b"", headonly=True)

        resp.headers.extend(
            [
                (b"etag", file.info.etag),
                (b"cache-control", bytesify("max-age=86400")),
                (b"last-modified", file.info.mtime_header),
            ]
        )

        try:
            matched = not file.have_no_match()
        except KeyError as e:
            self.logger.debug("no cache-freshness header: {}".format(e))
        else:
            if matched:
                await _response_304(resp)
                return

        try:
            without_modified = not file.have_modified_since()
        except KeyError as e:
            self.logger.debug("no cache-freshness header: {}".format(e))
        else:
            if without_modified:
                await _response_304(resp)
                return

        for h in basic_response_headers():  # pylint: disable=invalid-name
            resp.headers.insert(0, h)

        resp.headers.append((b"content-type", bytesify(file.content_type)))
        resp.headers.append((b"content-length", bytesify(str(file.info.size))))
        # TODO@haoliang support range header
        resp.headers.append((b"accept-ranges", b"none"))

        await self.send(resp)
        assert self.conn.our_state == h11.SEND_BODY

        if not headonly:
            await self.sendfile(file.stdfile, 0, file.info.size)

        await self.send(h11.EndOfMessage())
        assert self.conn.our_state in (h11.DONE, h11.MUST_CLOSE)

    async def sendfile(self, file: pathlib.Path, offset=0, count=None):
        """
        keep tracking https://github.com/python-trio/trio/issues/45

        * total_size
        * offset, count
        """

        if count == 0:
            return

        sock: stdsocket = self.stream.socket._sock  # type: ignore
        assert isinstance(sock, stdsocket)

        def _sync_sendfile():
            with open(str(file), "rb") as fp:
                sock.sendfile(fp, offset, count)

        placeholder = FilePlaceholder(file, offset, count)
        passthrough = self.conn.send_with_data_passthrough(h11.Data(data=placeholder))

        # TODO@haoliang test for >=1G
        self.logger.info("sending file in %s chunks", len(passthrough))
        assert len(passthrough) == 1

        # TODO@haoliang wrapper on os.sendfile to support non-blocking sock
        need_changing = not sock.getblocking()
        if need_changing:
            sock.setblocking(True)
        try:
            await trio.to_thread.run_sync(_sync_sendfile)
        finally:
            if need_changing:
                sock.setblocking(False)


class RequestProcessor:
    def __init__(self, communicator: Communicator, logger: Logger):
        self.communicator = communicator
        self.logger = logger

    async def process(self, request: h11.Request):
        raise NotImplementedError()


ProcessorFactory = Callable[[Communicator, Logger], RequestProcessor]


class StreamHandler:
    def __init__(self, processor_factory: ProcessorFactory):
        self._processor_factory = processor_factory
        # TODO@haoliang control number of RequestProcessor/stream/connection: dropping or waitting
        #       * apis raises sendfile/io operation has less number
        #       * apis raises purely computing operation has more number

    def _context(self, stream: trio.SocketStream):
        logger = Logger()
        communicator = Communicator(stream, logger)
        processor = self._processor_factory(communicator, logger)

        return logger, communicator, processor

    async def handle(self, stream: trio.SocketStream):
        """
        as trio._highlevel_serve_listeners._run_handler will close stream eventually,
        what we need to do is ensure our loop wont be infinite
        """

        logger, communicator, processor = self._context(stream)

        # hardcoded
        # same as keepalive-timeout
        event_wait_timeout = 2 * 60
        # TODO@haoliang response_timeout = read_request + read_response
        response_timeout = 60

        logger.info("Got new connection")

        while True:
            logger.info("mainloop waiting for request")

            assert communicator.conn.states == {
                h11.CLIENT: h11.IDLE,
                h11.SERVER: h11.IDLE,
            }

            try:
                with trio.fail_after(event_wait_timeout):
                    event = await communicator.next_event()
            except trio.BrokenResourceError:
                await communicator.force_shutdown()
                break
            except h11.RemoteProtocolError as exc:
                await communicator.shutdown()
                logger.exception(exc)
                break
            except trio.TooSlowError:
                logger.info("no incomming event, disconnecting")
                await communicator.shutdown()
                break
            else:
                logger.info("mainloop got event: %s", event)

            if isinstance(event, h11.Request):
                try:
                    with trio.fail_after(response_timeout):
                        await processor.process(event)
                except h11.RemoteProtocolError as exc:
                    await communicator.shutdown()
                    logger.exception(exc)
                    break
                except trio.BrokenResourceError as exc:
                    logger.exception(exc)
                    await communicator.force_shutdown()
                    break
                except trio.TooSlowError as exc:
                    logger.exception(exc)
                    headonly = event.method == b"HEAD"
                    resp = h11.Response(status_code=504, reason=b"Gateway Timeout")
                    await communicator.send_response(
                        resp, b"response timeout", headonly
                    )
                    await communicator.shutdown()
                    break
            else:
                assert isinstance(event, (h11.ConnectionClosed,))
                logger.info("unexpected event: %s", event)

            if communicator.conn.our_state is h11.MUST_CLOSE:
                logger.info("server in MUST_CLOSE state")
                await communicator.shutdown()
                break

            try:
                communicator.conn.start_next_cycle()
            except h11.LocalProtocolError as exc:
                await communicator.shutdown()
                logger.debug(repr(exc))
                break  # since that's an unrecoverable error from our side

    __call__ = handle
