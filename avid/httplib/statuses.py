"""
see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
"""


from typing import List, Tuple

import h11

from .typing import Response, bytesify

# uses http.HTTPStatus instead
# stolen from werkzeug.http
HTTP_STATUS_CODES = {
    code: bytesify(reason)
    for code, reason in {
        100: "Continue",
        101: "Switching Protocols",
        102: "Processing",
        103: "Early Hints",  # see RFC 8297
        200: "OK",
        201: "Created",
        202: "Accepted",
        203: "Non Authoritative Information",
        204: "No Content",
        205: "Reset Content",
        206: "Partial Content",
        207: "Multi Status",
        208: "Already Reported",  # see RFC 5842
        226: "IM Used",  # see RFC 3229
        300: "Multiple Choices",
        301: "Moved Permanently",
        302: "Found",
        303: "See Other",
        304: "Not Modified",
        305: "Use Proxy",
        306: "Switch Proxy",  # unused
        307: "Temporary Redirect",
        308: "Permanent Redirect",
        400: "Bad Request",
        401: "Unauthorized",
        402: "Payment Required",  # unused
        403: "Forbidden",
        404: "Not Found",
        405: "Method Not Allowed",
        406: "Not Acceptable",
        407: "Proxy Authentication Required",
        408: "Request Timeout",
        409: "Conflict",
        410: "Gone",
        411: "Length Required",
        412: "Precondition Failed",
        413: "Request Entity Too Large",
        414: "Request URI Too Long",
        415: "Unsupported Media Type",
        416: "Requested Range Not Satisfiable",
        417: "Expectation Failed",
        418: "I'm a teapot",  # see RFC 2324
        421: "Misdirected Request",  # see RFC 7540
        422: "Unprocessable Entity",
        423: "Locked",
        424: "Failed Dependency",
        425: "Too Early",  # see RFC 8470
        426: "Upgrade Required",
        428: "Precondition Required",  # see RFC 6585
        429: "Too Many Requests",
        431: "Request Header Fields Too Large",
        449: "Retry With",  # proprietary MS extension
        451: "Unavailable For Legal Reasons",
        500: "Internal Server Error",
        501: "Not Implemented",
        502: "Bad Gateway",
        503: "Service Unavailable",
        504: "Gateway Timeout",
        505: "HTTP Version Not Supported",
        506: "Variant Also Negotiates",  # see RFC 2295
        507: "Insufficient Storage",
        508: "Loop Detected",  # see RFC 5842
        510: "Not Extended",
        511: "Network Authentication Failed",  # see RFC 6585
    }.items()
}

Headers = List[Tuple[bytes, bytes]]


class HTTPError(Exception):
    status_code = 500

    def __init__(self, body=b"", headers: Headers = None, **extra_headers):
        super().__init__()

        assert isinstance(body, bytes)

        extra_headers["content-length"] = str(len(body))
        if headers is None:
            headers = []
        headers.extend((bytesify(h), bytesify(v)) for h, v in extra_headers.items())

        self._body = body
        self._headers = headers

    def as_response(self) -> Response:
        resp = h11.Response(
            status_code=self.status_code,
            headers=self._headers,
            http_version="1.1",
            reason=HTTP_STATUS_CODES[self.status_code],
        )

        return resp, self._body


class ArbitraryHTTPError(HTTPError):
    def __init__(
        self, status_code: int, body=b"", headers: Headers = None, **extra_headers
    ):
        super().__init__(body, headers, **extra_headers)

        self.status_code = status_code


class BadRequest(HTTPError):
    status_code = 400


class PayloadTooLarge(HTTPError):
    status_code = 413


class LengthRequired(HTTPError):
    status_code = 411


class MethodNotAllowed(HTTPError):
    status_code = 405

    def __init__(self, allow: str, body=b"", headers=None, **extra_headers):
        """
        :param allow: str, eg: `POST,GET`
        """
        extra_headers["allow"] = allow

        super().__init__(body, headers, **extra_headers)


class NotFound(HTTPError):
    status_code = 404


class NotModified(HTTPError):
    status_code = 304

    def __init__(
        self, etag, cache_control, body=b"", headers: Headers = None, **extra_headers
    ):
        extra_headers["etag"] = etag
        extra_headers["cache-control"] = cache_control

        super().__init__(body, headers, **extra_headers)


class Unauthorized(HTTPError):
    status_code = 401


class UnsupportedMediaType(HTTPError):
    status_code = 415


class RequestTimeout(HTTPError):
    status_code = 408
