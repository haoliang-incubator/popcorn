import hashlib
from os import stat_result
from typing import List, Optional

import attr
import h11
import trio
from cached_property import cached_property
from h11._headers import get_comma_header

from . import httpdate


@attr.s(frozen=True)
class FilePlaceholder:
    """
    used by h11; see h11._writers.*Writer.send_data
    """

    file = attr.ib()
    offset: int = attr.ib()
    count: int = attr.ib()

    def __len__(self):
        return self.count


@attr.s(frozen=True)
class FileInfo:
    file: trio.Path = attr.ib()  # absolute path
    stat: stat_result = attr.ib()
    mime: bytes = attr.ib()
    encoding: Optional[bytes] = attr.ib()
    size: int = attr.ib(init=False)  # in bytes
    mtime: httpdate.UTCDatetime = attr.ib(init=False)
    mtime_header: bytes = attr.ib(init=False)
    etag: bytes = attr.ib(init=False)

    @size.default
    def _default_size(self):
        return self.stat.st_size

    @mtime.default
    def _default_mtime(self):
        return httpdate.from_local_timestamp(self.stat.st_mtime)

    @mtime_header.default
    def _default_mtime_header(self):
        return httpdate.as_header(self.mtime)

    @etag.default
    def _default_etag(self):
        return make_etag(self.stat)


class SendFile:
    def __init__(self, file: FileInfo, request: h11.Request):
        self.info = file
        self._request = request

        self.stdfile = self.info.file._wrapped  # type: ignore

    def have_no_match(self) -> bool:
        given: List[bytes] = get_comma_header(self._request.headers, b"if-none-match")
        if not given:
            raise KeyError("no if-none-match header")

        hold = self.info.etag

        return hold not in given

    def have_modified_since(self) -> bool:
        hk: bytes
        hv: bytes
        for hk, hv in self._request.headers:
            if hk == b"if-modified-since":
                given_mtime: str = hv.decode()
                break
        else:
            raise KeyError("no if-modified-since header")

        # TODO@haoliang error handle
        given = httpdate.from_string(given_mtime)
        hold = self.info.mtime

        return hold > given

    @cached_property
    def content_type(self) -> bytes:
        if self.info.encoding is None:
            return self.info.mime

        return b"%b; charset=%b" % (self.info.mime, self.info.encoding)

    def __str__(self):
        return str(self.stdfile)

    def __repr__(self):
        return "<{}: {}>".format(self.__class__.__name__, str(self))


def make_etag(stat: stat_result) -> bytes:
    # stolen from bottle.py
    string = "{}:{}:{}:{}".format(stat.st_dev, stat.st_ino, stat.st_mtime, stat.st_size)
    return hashlib.sha1(string.encode()).hexdigest().encode("ascii")
