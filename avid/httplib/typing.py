from typing import AsyncIterable, Tuple

import h11

from .sendfile import FileInfo

Response = Tuple[h11.Response, bytes]
StreamResponse = Tuple[h11.Response, AsyncIterable[bytes]]
FileResponse = Tuple[h11.Response, FileInfo]
RequestBody = AsyncIterable[h11.Data]


def bytesify(obj):
    # partially stolen from h11._util.bytesify

    if isinstance(obj, (bytes, bytearray)):
        return obj

    if isinstance(obj, str):
        return obj.encode("ascii")

    if isinstance(obj, int):
        raise TypeError("expected bytes-like object, not int")

    return bytes(obj)
