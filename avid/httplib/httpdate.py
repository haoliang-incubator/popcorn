"""
see: https://tools.ietf.org/html/rfc7231#section-7.1.1.1
"""

import datetime
import time
import wsgiref.handlers
from typing import Union

FORMAT = "%a, %d %b %Y %H:%M:%S GMT"
UTCDatetime = datetime.datetime


def from_local_now() -> UTCDatetime:
    return datetime.datetime.utcnow()


def from_local_timestamp(timestamp: Union[float, int]) -> UTCDatetime:
    """
    精确到秒，小数部分忽略不计
    """
    timestamp = int(timestamp)
    return datetime.datetime.utcfromtimestamp(timestamp)


def from_local_datetime(dt: datetime.datetime) -> UTCDatetime:
    delta = datetime.timedelta(seconds=time.timezone)
    return dt + delta


def from_string(string: str) -> UTCDatetime:
    string = string.lower()
    assert string.endswith(" gmt")

    return datetime.datetime.strptime(string, FORMAT)


def as_string(utcdt: UTCDatetime) -> str:
    return utcdt.strftime(FORMAT)


def as_header(utcdt: UTCDatetime) -> bytes:
    return as_string(utcdt).lower().encode("ascii")


def now_as_header() -> bytes:
    return now_as_string().lower().encode("ascii")


def now_as_string() -> str:
    return wsgiref.handlers.format_date_time(None)
