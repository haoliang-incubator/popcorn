"""
ensure logging.config working inside avid.*
    * import avid.* inside functions
"""

import argparse
import logging
import socket

import trio
from rich.logging import RichHandler


def _processor_factory():
    # pylint: disable=import-outside-toplevel
    from avid.ui.web.processor import RequestProcessor
    from avid.ui.webtests.routes.registry import routes

    def instance(*args, **kwargs):
        return RequestProcessor(routes, *args, **kwargs)

    return instance


def _custom_sockopt(sl: trio.SocketListener):
    sl.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
    # # prerequires: sysctl net/ipv4/tcp_available_congestion_control # output contains `bbr`
    # sl.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_CONGESTION, b"bbr")

    # trio.open_tcp_listeners() already set this
    # sl.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    return sl


async def serve(ports: list):
    # pylint: disable=import-outside-toplevel
    from avid.httplib import StreamHandler

    logging.info("listening on http://localhost:%s", ports)

    handler = StreamHandler(_processor_factory())

    listeners: list = []
    for port in ports:
        listeners.extend(map(_custom_sockopt, await trio.open_tcp_listeners(port)))

    try:
        await trio.serve_listeners(
            handler, listeners, task_status=trio.TASK_STATUS_IGNORED
        )
    except KeyboardInterrupt:
        logging.info("KeyboardInterrupt - shutting down")


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", action="store_true")

    return parser.parse_args()


async def main():
    # pylint: disable=import-outside-toplevel
    from avid.ui.webtests import facts

    await facts.fulfill()
    await serve([8082])


if __name__ == "__main__":
    args = _parse_args()

    logging.basicConfig(
        level="WARNING" if args.quiet else "DEBUG",
        style="{",
        datefmt="%H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )
    logging.getLogger("multipart").setLevel(logging.WARNING)

    trio.run(main)
