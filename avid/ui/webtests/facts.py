import trio

from avid.ui.web import assets as _assets_types


class Filesystem:
    public: trio.Path = None  # type: ignore
    assets: trio.Path = None  # type: ignore
    uploads: trio.Path = None  # type: ignore
    temp: trio.Path = None  # type: ignore


class CORS:

    # see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Origin
    # pattern: <scheme> "://" <hostname> [ ":" <port> ]
    allow_origin = b"*"

    allow_headers = b",".join(
        [b"content-type", b"connection", b"accept", b"authorzation"]
    )

    max_age = str(60 * 60 * 24).encode("ascii")


Assets: _assets_types.Assets = None  # type: ignore


async def fulfill():
    global Assets

    selfile = await trio.Path(__file__).resolve()
    Filesystem.public = selfile.parent.joinpath("public")
    Filesystem.assets = Filesystem.public.joinpath("assets")
    Filesystem.uploads = Filesystem.public.joinpath("uploads")
    tempdir = trio.Path("/tmp/avidtemp")
    Filesystem.temp = tempdir
    await tempdir.mkdir(mode=0o777, exist_ok=True)

    Assets = await _assets_types.AssetsFactory.from_paths(Filesystem.assets)
