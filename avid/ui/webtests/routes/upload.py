"""
todo: parser unified data container access entry?
"""

import logging

import h11
from avid.httplib.formparser import (
    ContentDisposition,
    Form,
    Headers,
    MultipartParser,
    UploadFile,
)
from avid.ui.web import response

_log = logging.getLogger(__name__)


async def upload(request: h11.Request, body: Form):
    headers = Headers(request.headers)
    ctype = headers.content_type

    payload = {
        "method": request.method.decode(),
        "target": request.target.decode(),
        "headers": [(name.decode(), value.decode()) for name, value in request.headers],
    }

    if ctype.startswith(MultipartParser.CONTENT_TYPE):
        _log.debug("boundary: %s", headers.boundary)
        _log.debug("multipart/form-data: %s", body)

    payload["file"] = []
    payload["form"] = []
    cd: ContentDisposition
    file: UploadFile
    for cd, *rest in body:
        cd_repr = str({k.decode(): v.decode() for k, v in cd.items()})
        if len(rest) == 2:
            ctype, file = rest
            payload["file"].append((cd_repr, ctype.decode(), str(file._file)))
        else:
            (ctype,) = rest
            payload["form"].append((cd_repr, ctype.decode()))

    return response.json(payload)
