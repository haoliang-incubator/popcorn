# pylint: disable=unused-argument

import datetime
import logging
from itertools import chain

import h11
import trio
from avid.httplib.formparser import Form
from avid.ui.web import decos, response
from avid.ui.web.assets import AssetsFactory
from avid.ui.web.ingress import (
    BareIngress,
    FormMultipartIngress,
    FormUrlencodedIngress,
    JsonIngress,
)
from avid.ui.web.router import Router

from .. import facts
from . import echo, upload

_log = logging.getLogger(__name__)


# vendors #{{{
async def ping(request, body):
    return response.text("pong")


class Counter:
    def __init__(self):
        self.current = 0

    async def next(self, request):
        self.current += 1

        return response.text(str(self.current))

    async def reset(self, request):
        self.current = 0

        return response.text(str(self.current))


count = Counter()


async def tickn(request, body, times: int):
    async def _tick(end: int, interval=1):
        _nowfunc = datetime.datetime.now

        for _ in range(end):
            await trio.sleep(interval)
            yield _nowfunc().strftime("%Y-%m-%d %H:%M:%S\r\n").encode()

    return response.stream(
        _tick(times), **{"content-type": "plain/text; charset=utf-8"}
    )


async def data(request, body):
    file = facts.Filesystem.public / "data"

    if not await file.exists():
        _infp = trio.open_file("/dev/urandom", "rb")
        _outfp = trio.open_file(file, "wb")
        async with await _infp as infp, await _outfp as outfp:
            await outfp.write(await infp.read(1024))

    fileinfo = await AssetsFactory._file_info(file)

    return response.file(fileinfo)


async def now(request):
    local = datetime.datetime.now()
    utc = datetime.datetime.utcnow()

    return response.json(
        {
            "local": local.strftime("%Y-%m-%d %H:%M:%S"),
            "local-timestamp": local.timestamp(),
            "utc": utc.strftime("%Y-%m-%d %H:%M:%S"),
            "utc-timestamp": utc.timestamp(),
        }
    )


# #}}}

# retistry #{{{
routes = Router()


def _with_ingress(endpoint, ingress):
    async def attached(*args, **kwargs):
        return await endpoint(*args, **kwargs)

    attached.ingress = ingress

    return attached


def _with_bare_ingress(endpoint):
    return _with_ingress(endpoint, BareIngress())


def _add_with_cors(rule, method_target: dict):
    """
    :param method_target: dict[str, async? callable]
    """

    allow_methods = ",".join(chain(method_target.keys(), ("OPTIONS",))).encode()

    async def preflight_endpoint(*args, **kwargs):
        return response.cors_preflight(allow_methods)

    routes.add(rule, "OPTIONS", preflight_endpoint)

    for method, target in method_target.items():
        endpoint = decos.cors_depart(target)
        routes.add(rule, method, endpoint)


_ping = _with_bare_ingress(ping)
_add_with_cors("/cors", {"GET": _ping})

routes.add("/count", "GET", _with_bare_ingress(count.next))
routes.add("/count", "DELETE", _with_bare_ingress(count.reset))

_add_with_cors(
    "/echo",
    {
        "GET": _with_bare_ingress(echo.bare),
        "POST": _with_ingress(echo.json_payload, JsonIngress()),
        "PUT": _with_ingress(echo.json_payload, JsonIngress()),
        "DELETE": _with_bare_ingress(echo.bare),
    },
)

routes.add("/", "GET", _ping)
routes.add("/ping", "GET", _ping)

routes.add("/tick/<times:int>", "GET", _with_bare_ingress(tickn))

_echo_url_args = _with_bare_ingress(echo.url_args)
routes.add("/av/<id:int>", "GET", _echo_url_args)
routes.add("/av/<id:int>/favorite", "PUT", _echo_url_args)
routes.add("/av/<id:int>/favorite", "DELETE", _echo_url_args)
routes.add("/av/<id:int>/more", "PUT", _echo_url_args)
routes.add("/av/<id:int>/more", "DELETE", _echo_url_args)

_auth_now = decos.auth_basic("mordor", haoliang="passwd", frodo="friend")(now)
routes.add("/now", "GET", _with_bare_ingress(_auth_now))

routes.add("/data", "GET", _with_bare_ingress(data))

routes.add(
    "/upload",
    "POST",
    _with_ingress(
        upload.upload,
        FormMultipartIngress(
            allow_uploading=True,
            allow_mimetypes=frozenset((b"application/octet-stream", b"text/plain")),
        ),
    ),
)


routes.add("/echo.json", "POST", _with_ingress(echo.json_payload, JsonIngress()))
routes.add(
    "/echo.form", "POST", _with_ingress(echo.form_payload, FormUrlencodedIngress())
)


# #}}}
