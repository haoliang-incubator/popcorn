# pylint: disable=unused-argument

import h11
from avid.httplib.formparser import ContentDisposition, Form, UploadFile
from avid.ui.web import response


def json_payload(request: h11.Request, body):
    resp = {
        "method": request.method.decode(),
        "target": request.target.decode(),
        "headers": [(name.decode(), value.decode()) for name, value in request.headers],
        "json": body,
    }

    return response.json(resp)


def form_payload(request: h11.Request, body: Form):
    resp = {
        "method": request.method.decode(),
        "target": request.target.decode(),
        "headers": [(name.decode(), value.decode()) for name, value in request.headers],
        "form": [],
        "files": [],
    }

    cd: ContentDisposition
    file: UploadFile

    for cd, *rest in body:
        cd_repr = str({k.decode(): v.decode() for k, v in cd.items()})
        if len(rest) == 2:
            ctype, file = rest
            resp["file"].append((cd_repr, ctype.decode(), str(file._file)))
        else:
            (ctype,) = rest
            resp["form"].append((cd_repr, ctype.decode()))

    return response.json(resp)


def url_args(request: h11.Request, body, **kwargs):
    resp = {
        "method": request.method.decode(),
        "target": request.target.decode(),
        "headers": [(name.decode(), value.decode()) for name, value in request.headers],
        "kwargs": kwargs,
    }

    return response.json(resp)


def bare(request: h11.Request, body):
    resp = {
        "method": request.method.decode(),
        "target": request.target.decode(),
        "headers": [(name.decode(), value.decode()) for name, value in request.headers],
    }

    return response.json(resp)
