from typing import Any, Callable, Dict, Tuple

import bottle
import h11
import hyperlink

from avid.httplib.statuses import MethodNotAllowed, NotFound


class Router:
    def __init__(self):
        self._router = bottle.Router()

    def add(self, rule, method: str, target, name=None):
        """
        :param target: callable or `async callable`
        """
        self._router.add(rule, method, target, name)

    def match(self, request: h11.Request) -> Tuple[Callable, Dict[str, Any]]:
        url = hyperlink.parse(request.target.decode("ascii"), lazy=True).normalize()
        path = "/" + "/".join(url.path)

        environ = {
            "PATH_INFO": path,
            "REQUEST_METHOD": request.method.decode(),
        }

        try:
            return self._router.match(environ)
        except bottle.HTTPError as err:
            headers = err.headers

            if err.status_code == 404:
                raise NotFound(err.body.encode(), **headers) from err

            if err.status_code == 405:
                allow = headers.pop("Allow")
                raise MethodNotAllowed(allow, err.body.encode()) from err

            raise RuntimeError(
                "unexpected bottle httperror in bottle.router.match"
            ) from err
