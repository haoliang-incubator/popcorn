"""
GET/HEAD/OPTIONS/DELETE request:
* has no content-type but accept

data source in POST/PUT request:
* ctype: application/json
* ctype: application/x-www-form-urlencoded
* ctype: multipart/form-data; ...

定位: 在endpoint执行前, 处理尚未读入内存的 request body

可以采取的处理措施:
* 读取部分 request body
    * 拒绝进入 endpoint (raise exceptions: http, protocol)
        * 注意：这种情况下 http connection 是不可重用的
* 读取完整 request body (见 form-parsers)
    * 对不断读入的 request body 进行验证
    * 对读入内存的 request body 进行转换: bytes->json, form->dict
"""

from typing import FrozenSet

import attr

# TODO@haoliang samephore/token/capacity limit number of connections on an Ingress


class Ingress:
    methods: FrozenSet[bytes] = frozenset()
    max_body_size = 0


class BareIngress(Ingress):
    """bare: no body """

    # {PUT,POST,DELETE} /resource/1/favorite
    methods: FrozenSet[bytes] = frozenset(
        (b"GET", b"HEAD", b"OPTIONS", b"DELETE", b"PUT", b"POST")
    )
    body_read_timeout = 1
    max_body_size = 0


@attr.s(frozen=True)
class JsonIngress(Ingress):
    methods: FrozenSet[bytes] = frozenset((b"POST", b"PUT"))
    content_type: bytes = b"application/json"
    max_body_size: int = attr.ib(default=8 << 10)
    body_read_timeout: float = attr.ib(default=60)  # unit: second


@attr.s(frozen=True)
class FormUrlencodedIngress(Ingress):
    methods: FrozenSet[bytes] = frozenset((b"POST", b"PUT"))
    content_type: bytes = b"application/x-www-form-urlencoded"
    max_body_size: int = attr.ib(default=8 << 10)
    body_read_timeout: float = attr.ib(default=60)  # unit: second


@attr.s(frozen=True)
class FormMultipartIngress(Ingress):
    methods: FrozenSet[bytes] = frozenset((b"POST", b"PUT"))
    content_type: bytes = b"multipart/form-data"
    max_body_size: int = attr.ib(default=10 << 20)  # unit: byte
    body_read_timeout: float = attr.ib(default=60)  # unit: second
    allow_uploading: bool = attr.ib(default=False)
    max_uplods: int = attr.ib(default=5)
    allow_mimetypes: FrozenSet[bytes] = attr.ib(factory=frozenset)
