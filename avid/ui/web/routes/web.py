# pylint: disable=unused-argument

from avid.httplib.statuses import NotFound
from avid.ui.web import facts, response


async def index(request):  # pylint: disable=unused-argument
    html = facts.Filesystem.public.joinpath("index.html")

    async with await html.open("rt") as fp:
        content = await fp.read()

    return response.html(content)


async def staticfile(request, filename: str):  # pylint: disable=unused-argument
    file = facts.Filesystem.assets.joinpath(filename)

    try:
        fileinfo = facts.Assets.file_info(str(file))
    except FileNotFoundError as exc:
        raise NotFound() from exc

    return response.file(fileinfo)


async def ping(request):
    return response.text("pong")
