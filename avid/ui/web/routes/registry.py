# pylint: disable=unused-argument

from itertools import chain

from .. import decos, response
from ..ingress import BareIngress
from ..router import Router
from . import web

routes = Router()


def _add_with_cors(rule, method_target: dict):
    """
    :param method_target: dict[str, async? callable]
    """

    allow_methods = ",".join(chain(method_target.keys(), ("OPTIONS",))).encode()

    async def preflight_endpoint(*args, **kwargs):
        return response.cors_preflight(allow_methods)

    routes.add(rule, "OPTIONS", preflight_endpoint)

    for method, target in method_target.items():
        endpoint = decos.cors_depart(target)
        routes.add(rule, method, endpoint)


def _with_ingress(endpoint, ingress):
    async def attached(*args, **kwargs):
        return await endpoint(*args, **kwargs)

    attached.ingress = ingress

    return attached


routes.add("/", "GET", _with_ingress(web.index, BareIngress()))
routes.add("/ping", "GET", _with_ingress(web.ping, BareIngress()))
routes.add(
    "/assets/<filename:path>", "GET", _with_ingress(web.staticfile, BareIngress())
)
