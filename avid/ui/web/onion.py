from typing import Callable, Iterable

# TODO@haoliang async callable/awaitable


class Onion:
    """
    pre:
        def a_pre_func(endpoint):
            @functools.wraps(endpoint)
            def wrapper(*args, **kwargs):
                ...
                print('do my job')
                ...
                return wrapped()
            return wrapper

    post:
        def a_post_func(endpoint):
            @functools.wraps(endpoint)
            def wrapper(*args, **kwargs):
                result = wrapped()
                ...
                print('do my job')
                ...
                return result
            return wrapper

    """

    def __init__(self, pres: Iterable[Callable], posts: Iterable[Callable]):
        self._pres = tuple(pres)
        self._posts = tuple(posts)

        self._flow = self._posts + self._pres[::-1]

    @property
    def flow(self):
        return self._flow

    def wrap(self, endpoint: Callable, excludes: Iterable[str] = None):
        """
        :param endpoint: api endpoint
        :param excludes: excludes {pre,post}.__name__
        """

        excludes = set(excludes or ())
        wrapped = endpoint

        for func in self._flow:
            if func.__name__ in excludes:
                continue
            wrapped = func(wrapped)

        return wrapped

    def deco(self, *, excludes: Iterable[str] = None):
        def for_wrap(endpoint):
            return self.wrap(endpoint, excludes)

        return for_wrap
