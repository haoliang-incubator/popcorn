# pylint: disable=unused-argument

import base64
import functools
import logging

from avid.httplib.statuses import Unauthorized

from . import facts, response

_log = logging.getLogger(__name__)


def cors_preflight(allow_methods=b"OPTIONS"):
    """
    see: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

    :param allow_methods: bytes, comma separated methods
    """

    def endpoint_wrapper(endpoint):
        @functools.wraps(endpoint)
        async def invoker(request, body, *args, **kwargs):
            # TODO@haoliang exhaust request body
            return response.cors_preflight(allow_methods)

        return invoker

    return endpoint_wrapper


def cors_depart(endpoint):
    @functools.wraps(endpoint)
    async def invoker(*args, **kwargs):
        resp, body = await endpoint(*args, **kwargs)

        resp.headers.append((b"access-control-allow-origin", facts.CORS.allow_origin))

        return resp, body

    return invoker


def auth_basic(realm: str, **pairs):
    """
    :param pairs: List[Tuple[str, str]]
    """

    def gencred(user: str, passwd: str):
        return base64.b64encode("{}:{}".format(user, passwd).encode("utf-8"))

    credentials = {gencred(user, passwd) for user, passwd in pairs.items()}
    scheme_prefix = b"Basic "

    def unauthorized():
        raise Unauthorized(
            **{"www-authenticate": f'Basic realm="{realm}", charset="UTF-8"'}
        )

    def endpoint_wrapper(endpoint):
        @functools.wraps(endpoint)
        async def invoker(request, *args, **kwargs):
            hv: bytes
            for hk, hv in request.headers:
                if hk == b"authorization":
                    msg = hv
                    break
            else:
                raise unauthorized()

            if not msg.startswith(scheme_prefix):
                raise unauthorized()

            given_cred = msg[len(scheme_prefix) :]
            if given_cred not in credentials:
                _log.debug("incorrect credential for endpoint: %s", endpoint.__name__)
                raise unauthorized()

            return await endpoint(request, *args, **kwargs)

        return invoker

    return endpoint_wrapper
