"""
endpoint signature
    awaitable(
        request h11.Request,
        body: Form|json|b"",
        **keywords_in_route
    ) -> Tuple[h11.Response, Union[bytes, AsyncIterable[bytearray]]]
"""

import inspect
import json
from contextlib import AsyncExitStack

import avid.httplib
import h11
import trio
from avid.httplib import (
    Communicator,
    FileInfo,
    HTTPError,
    Logger,
    RequestBody,
    SendFile,
)
from avid.httplib.formparser import Form, Headers, MultipartParser, QuerystringParser
from avid.httplib.statuses import (
    BadRequest,
    LengthRequired,
    MethodNotAllowed,
    NotFound,
    PayloadTooLarge,
    RequestTimeout,
    UnsupportedMediaType,
)

from .ingress import (
    BareIngress,
    FormMultipartIngress,
    FormUrlencodedIngress,
    Ingress,
    JsonIngress,
)
from .router import Router


class RequestProcessor(avid.httplib.RequestProcessor):
    def __init__(
        self, routes: Router, communicator: Communicator, logger: Logger,
    ):
        super().__init__(communicator, logger)

        self.routes = routes

    async def process(self, request: h11.Request):
        communicator = self.communicator

        head_request = request.method == b"HEAD"

        try:
            endpoint, url_args = self.routes.match(request)
        except (NotFound, MethodNotAllowed) as err:
            # disregard the request body, close the connection
            resp, body = err.as_response()
            resp.headers.append((b"connection", b"close"))
            await communicator.send_response(resp, body, headonly=head_request)
            return

        try:
            ingress = getattr(endpoint, "ingress")
        except AttributeError as err:
            raise RuntimeError("endpoint lack of ingress") from err
        else:
            self.logger.debug("ingress: %s", ingress)

        try:
            approved_body = await self._approve(request, ingress)
        except HTTPError as err:
            # disregard the request body, close the connection
            self.logger.exception(err)
            resp, body = err.as_response()
            resp.headers.append((b"connection", b"close"))
            await communicator.send_response(resp, body, headonly=head_request)
            return

        self.logger.info("invoking %s with args %s", endpoint, url_args)

        try:
            # TODO@haoliang response read-timeout
            if isinstance(approved_body, Form):
                stack: AsyncExitStack
                async with AsyncExitStack() as stack:
                    for file in approved_body.files():
                        await stack.enter_async_context(file)
                    resp, body = await endpoint(
                        request=request, body=approved_body, **url_args
                    )
            else:
                resp, body = await endpoint(
                    request=request, body=approved_body, **url_args
                )

        except HTTPError as err:
            self.logger.exception(err)
            await communicator.send_response(*err.as_response(), headonly=head_request)
            return
        else:
            assert isinstance(resp, h11.Response), "unsupported response type"

        self.logger.info("every thing in processor is fine.")

        if isinstance(body, (bytes, bytearray)):
            await communicator.send_response(resp, body, headonly=head_request)
            return

        if inspect.isasyncgen(body):
            # TODO@haoliang should also be considered in response read-timeout
            await communicator.send_stream_response(resp, body, headonly=head_request)
            return

        if isinstance(body, FileInfo):
            await communicator.send_file_response(
                resp, SendFile(body, request), headonly=head_request
            )
            return

        raise NotImplementedError(
            "unsupported body type: ({}){}".format(type(body), body)
        )

    async def _approve(self, request: h11.Request, ingress: Ingress):
        communicator = self.communicator

        request_without_body = ingress.max_body_size == 0

        if request_without_body:
            request_body = communicator.read_empty_request_body()
        else:
            request_body = communicator.read_request_body()

        if isinstance(ingress, BareIngress):
            return await Guard.approve_bare_ingress(ingress, request, request_body)

        if isinstance(ingress, JsonIngress):
            return await Guard.approve_json_ingress(ingress, request, request_body)

        if isinstance(ingress, FormUrlencodedIngress):
            return await Guard.approve_form_urlencoded_ingress(
                ingress, request, request_body
            )

        if isinstance(ingress, FormMultipartIngress):
            return await Guard.approve_form_multipart_ingress(
                ingress, request, request_body
            )

        raise RuntimeError("unsupported ingress")


class Guard:
    def __init__(self, ingress: Ingress, request: h11.Request, body: RequestBody):
        self._ingress = ingress
        self._request = request
        self._body = body

        # raw bytes
        self._raw = None
        self._json = None
        self._form: Form = None  # type: ignore
        self._multipart: Form = None  # type: ignore

    def _legal_method(self):
        if self._request.method not in self._ingress.methods:
            raise MethodNotAllowed(b",".join(self._ingress.methods))

    def _legal_content_type(self):

        for hk, hv in self._request.headers:
            if hk == b"content-type":
                # TODO@haoliang support `content-type: mimetype; charset ...`
                if not hv.startswith(self._ingress.content_type):
                    raise UnsupportedMediaType()
                break
        else:
            raise BadRequest(b"missing content-type header")

    def _legal_content_length(self):

        for hk, hv in self._request.headers:
            if hk != b"content-length":
                continue

            try:
                clen = int(hv)
            except ValueError as err:
                raise BadRequest(b"invalid content-length") from err

            if clen > self._ingress.max_body_size:
                raise PayloadTooLarge()

            break
        else:
            raise LengthRequired()

    async def _legal_raw_body(self):
        if self._raw is not None:
            return

        try:
            raw = bytearray()
            with trio.fail_after(self._ingress.body_read_timeout):
                max_body_size = self._ingress.max_body_size
                async for chunk in self._body:
                    if len(raw) > max_body_size:
                        raise PayloadTooLarge()
                    raw.extend(chunk.data)
        except trio.TooSlowError as err:
            raise RequestTimeout() from err
        else:
            self._raw = raw

    async def _legal_json_body(self):
        if self._json is not None:
            return

        await self._legal_raw_body()

        try:
            payload = json.loads(self._raw)
        except json.JSONDecodeError as err:
            raise BadRequest(b"malformed json payload") from err
        else:
            self._json = payload

    async def _legal_form_body(self):
        if self._form is not None:
            return

        parser = QuerystringParser(Headers(self._request.headers), self._body)

        try:
            with trio.fail_after(self._ingress.body_read_timeout):
                await parser.parse()
        except trio.TooSlowError as err:
            raise RequestTimeout() from err

        self._form = parser.pairs

    async def _legal_multipart_body(self):
        if self._multipart is not None:
            return

        # TODO@haoliang hardcode
        uploaddir = trio.Path("/tmp/avid.uploads")

        parser = GuardedMultipartParser(
            Headers(self._request.headers), self._body, uploaddir, self._ingress,
        )

        try:
            with trio.fail_after(self._ingress.body_read_timeout):
                await parser.parse()
        except trio.TooSlowError as err:
            raise RequestTimeout() from err

        self._multipart = parser.pairs

    @classmethod
    async def approve_json_ingress(
        cls, ingress: JsonIngress, request: h11.Request, body: RequestBody
    ):
        guard = cls(ingress, request, body)
        guard._legal_method()
        guard._legal_content_type()
        guard._legal_content_length()
        await guard._legal_json_body()

        return guard._json

    @classmethod
    async def approve_bare_ingress(
        cls, ingress: BareIngress, request: h11.Request, body: RequestBody
    ):
        guard = cls(ingress, request, body)
        guard._legal_method()
        await guard._legal_raw_body()

        return guard._raw

    @classmethod
    async def approve_form_urlencoded_ingress(
        cls, ingress: FormUrlencodedIngress, request: h11.Request, body: RequestBody
    ):
        guard = cls(ingress, request, body)
        guard._legal_method()
        guard._legal_content_type()
        guard._legal_content_length()
        await guard._legal_form_body()

        return guard._form

    @classmethod
    async def approve_form_multipart_ingress(
        cls, ingress: FormMultipartIngress, request: h11.Request, body: RequestBody
    ):

        guard = cls(ingress, request, body)
        guard._legal_method()
        guard._legal_content_type()
        guard._legal_content_length()
        await guard._legal_multipart_body()

        return guard._multipart


class GuardedMultipartParser(MultipartParser):
    def __init__(
        self,
        headers: Headers,
        source: RequestBody,
        uploaddir: trio.Path,
        ingress: FormMultipartIngress,
    ):
        super().__init__(headers, source, uploaddir)
        self._ingress = ingress

        self._upload_count = 0

    def _header_content_disposition(self, value: memoryview):
        super()._header_content_disposition(value)

        # pylint: disable=comparison-with-callable
        if self._data_handler == self._data_file_chunk:

            if not self._ingress.allow_uploading:
                raise BadRequest(b"disallow uploading")

            if self._upload_count > self._ingress.max_uplods:
                raise BadRequest(b"too many upload file")

            self._upload_count += 1

    def _header_content_type(self, value: memoryview):
        super()._header_content_type(value)
        ctype = self._pairs[self._cursor][-1]

        if ctype not in self._ingress.allow_mimetypes:
            raise BadRequest(b"disallow mimetype of upload file")
