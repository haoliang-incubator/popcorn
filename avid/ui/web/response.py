import json as _json
import logging
from typing import AsyncIterable

import h11

from avid.httplib import HTTP_STATUS_CODES, bytesify
from avid.httplib.typing import (FileInfo, FileResponse, Response,
                                 StreamResponse)

from . import facts

_log = logging.getLogger(__name__)


def text(string: str, status_code=200, **headers) -> Response:
    headers["content-type"] = "text/plain; charset=utf-8"
    body = string.encode("utf-8")

    return _response_with_length(body, status_code, headers)


def html(string: str, status_code=200, **headers) -> Response:
    headers["content-type"] = "text/html; charset=utf-8"
    body = string.encode("utf-8")

    return _response_with_length(body, status_code, headers)


def json(obj: object, status_code=200, **headers) -> Response:
    headers["content-type"] = "application/json; charset=utf-8"
    body = _json.dumps(obj, ensure_ascii=False).encode()

    return _response_with_length(body, status_code, headers)


def nocontent() -> Response:
    resp = h11.Response(
        status_code=204, headers=[], http_version=b"1.1", reason=b"No Content",
    )

    return resp, b""


def cors_preflight(allow_methods=b"OPTIONS") -> Response:
    headers = [
        (b"access-control-allow-origin", facts.CORS.allow_origin),
        (b"access-control-allow-methods", allow_methods),
        (b"access-control-allow-headers", facts.CORS.allow_headers),
        (b"access-control-max-age", facts.CORS.max_age),
    ]

    resp = h11.Response(
        status_code=204, headers=headers, http_version=b"1.1", reason=b"No Content",
    )

    return resp, b""


def file(file_: FileInfo, status_code=200) -> FileResponse:
    """
    currently no headers need to be specified
    """
    resp = h11.Response(
        status_code=status_code,
        headers=[],
        http_version=b"1.1",
        reason=HTTP_STATUS_CODES[status_code],
    )

    return resp, file_


def stream(source: AsyncIterable[bytes], status_code=200, **headers) -> StreamResponse:
    headers["transfer-encoding"] = "chunked"

    resp = h11.Response(
        status_code=status_code,
        headers=_normalize_headers(headers),
        http_version=b"1.1",
        reason=HTTP_STATUS_CODES[status_code],
    )

    return resp, source


def _response_with_length(body: bytes, status_code: int, headers: dict) -> Response:
    headers["content-length"] = str(len(body))

    resp = h11.Response(
        status_code=status_code,
        headers=_normalize_headers(headers),
        http_version=b"1.1",
        reason=HTTP_STATUS_CODES[status_code],
    )

    return resp, body


def _normalize_headers(headers: dict):
    return list((bytesify(h), bytesify(v)) for h, v in headers.items())
