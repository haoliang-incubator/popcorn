async function _echo() {
    const resp = await fetch('http://localhost:8081/echo', {
        method: "POST",
        headers: {
            "content-type": "application/json",
            // "x-o-x": "o-x-o", // server disallows this header
        },
        body: JSON.stringify({'hello': ['wo', 'rld']}),
    })

    return resp.json()
}

if (window.fetch) {
    _echo().then(function (jn) {
        console.log(jn)
    }).catch(function (err) {
        console.log(err)
    })
} else {
    alert("this fucking browser does not support fetch api!");
}
