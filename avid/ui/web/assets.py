import logging
from typing import AsyncIterable, Dict, Optional

import trio

from avid.httplib import FileInfo

_log = logging.getLogger(__name__)


class Assets:
    def __init__(self, db: Dict[str, FileInfo]):
        _log.debug("create Assets with db: %s", db)
        self.db = db

    def file_info(self, fullpath: str) -> FileInfo:
        try:
            return self.db[fullpath]
        except KeyError as exc:
            raise FileNotFoundError(fullpath) from exc

    def file_exists(self, fullpath: str) -> bool:
        return fullpath in self.db

    def __iter__(self):
        return iter(self.db.items())


class AssetsFactory:
    KNOWN_MIMES = {
        ".js": (b"text/javascript", b"utf-8"),
        ".js.map": (b"application/json", b"utf-8"),
        ".css": (b"text/css", b"utf-8"),
        ".json": (b"application/json", b"utf-8"),
        ".html": (b"text/html", b"utf-8"),
        ".py": (b"text/plain", b"utf-8"),
    }

    ALLOWED_SUFFIXES = {".js", ".css", ".html"}

    @classmethod
    async def from_paths(cls, *paths: trio.Path) -> Assets:
        async def process_one(path: trio.Path):
            assert path.is_absolute()

            return (
                (str(file), await cls._file_info(file))
                async for file in cls._find_files(path)
            )

        async def chain(asources: AsyncIterable[AsyncIterable]):
            async for asource in asources:
                async for item in asource:
                    yield item

        source = chain(await process_one(path) for path in paths)  # type: ignore

        return Assets({file: info async for file, info in source})

    @classmethod
    async def _find_files(cls, dir: trio.Path):
        for file in await dir.iterdir():
            if await file.is_file():
                if file.suffix in cls.ALLOWED_SUFFIXES:
                    yield file
                continue

            if await file.is_dir():
                async for _file in cls._find_files(file):
                    yield _file
                continue

    @classmethod
    async def _file_info(cls, file: trio.Path):
        # TODO@haoliang make AssetsFactory._file_info() public
        stat = await file.stat()

        encoding: Optional[bytes]

        try:
            # seems no need to use `python.mimetypes`
            mime, encoding = cls.KNOWN_MIMES[file.suffix]
        except KeyError:
            _log.warning("cannot determine mime type of %s", file)
            mime = b"application/octet-stream"
            encoding = None

        return FileInfo(file, stat, mime, encoding)
