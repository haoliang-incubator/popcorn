from typing import List

import trio


class Filesystem:
    var: trio.Path = None  # type: ignore
    videodb: trio.Path = None  # type: ignore
    avdb: trio.Path = None  # type: ignore
    libraries: List[trio.Path] = []


async def fulfill():

    selfile = await trio.Path(__file__).resolve()
    var = await selfile.joinpath("..", "..", "..", "..", "var").resolve()
    Filesystem.var = var
    Filesystem.videodb = var.joinpath("video.db")
    Filesystem.avdb = var.joinpath("av.db")

    lib: trio.Path
    Filesystem.libraries = [
        lib
        for lib in [
            trio.Path("/srv/av/final"),
            trio.Path("/mnt/lynel/av/av"),
            trio.Path("/mnt/pearl/av/av"),
            trio.Path("/mnt/zealot/av"),
            trio.Path("/srv/playground/avid/var/videos/slices"),
        ]
        if await lib.exists()
    ]
