import logging

import attr

from . import facts
from .entities import VideoDB

_log = logging.getLogger(__name__)


def main():
    videodb = VideoDB.from_file(facts.Filesystem.videodb)

    count = [0, 0]
    for video in videodb:
        try:
            _code(video.location)
        except Unidentifiable as e:
            _log.info(e.explain)
            count[1] += 1
        else:
            count[0] += 1
    _log.info("ident: %d; unident: %d", count[0], count[1])


def _all(db: VideoDB):
    for video in db:
        print(attr.asdict(video))


def _code(fullpath: str):
    """ series-no.mp4
    :raise: Unidentifiable

    general pattern
    * 200gana-xxx.mp4

    special cases - keyword
    * fhd-snis-943.mp4
    * fc2-ppv-880652.mp4
    * carib-051419-919.mp4
    * caribbeancom-041113-310.mp4
    * idbd-765-a.mp4
    * ssni-658-part1.mp4
    * ipz-677-hd.mp4

    special cases:
    * fc2_ppv_880652.mp4
    * heyzo_1811.mp4
    * 1pondo_032517_505.mp4
    """
    filename = fullpath[fullpath.rfind("/") + 1 :].lower()

    def _series_no(series: str, no: str):
        if no[-1] in ("a", "b", "c", "d"):
            no = no[:-1]

        try:
            int(no)
        except ValueError as e:
            raise Unidentifiable(filename, "int(no)") from e
        else:
            return series, no

    if not filename.endswith(".mp4"):
        raise Unidentifiable(filename, ".mp4")

    parts = filename[:-4].split("-")

    if len(parts) == 2:
        return _series_no(*parts)

    if len(parts) == 3:
        if parts[0] == "fhd":
            return _series_no(*parts[1:3])
        if parts[0] == "carib":
            return _series_no("carib", parts[1])
        if parts[0] == "caribbeancom":
            return _series_no("caribbeancom", parts[1])

        if parts[2] in ("a", "b", "c", "d"):
            return _series_no(*parts[:2])
        if parts[2].startswith("part"):
            return _series_no(*parts[:2])
        if parts[2] == "hd":
            return _series_no(*parts[:2])
        if parts[:2] == ("fc2", "ppv"):
            return _series_no("fc2_ppv", parts[2])

    raise Unidentifiable(filename, "unexpected")


class Unidentifiable(Exception):
    def __init__(self, filename, reason):
        super().__init__()
        self.explain = f"skipped due to {reason}: {filename}"
