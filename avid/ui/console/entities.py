import math
import pickle
from typing import List

import attr
import trio
from pympler import asizeof


@attr.s
class Video:
    # pylint: disable=invalid-name
    mime_type: str = attr.ib()
    fingerprint: str = attr.ib()
    born_at: float = attr.ib()
    location: str = attr.ib()
    size: int = attr.ib()
    resolution: str = attr.ib()  # widthxheight
    duration: str = attr.ib()  # HH:MM:ss


class VideoDB:
    def __init__(self, source: List[Video]):
        self._db = source

    def append(self, video: Video):
        self._db.append(video)

    def __len__(self):
        return len(self._db)

    def __iter__(self):
        return iter(self._db)

    def stats(self):
        memory_size = asizeof.asizeof(self)
        return {
            "total_videos": len(self._db),
            "used_memory": memory_size,
            "used_memory_human": readable_size(memory_size),
        }

    @classmethod
    def from_file(cls, file: trio.Path):
        try:
            with open(str(file), mode="rb") as fp:
                db = pickle.load(fp)
        except FileNotFoundError:
            db = []

        return cls(db)

    def to_file(self, file: trio.Path):
        with open(str(file), mode="wb") as fp:
            pickle.dump(self._db, fp)


def readable_size(size: int):
    """
    stolen from https://ourcodeworld.com/articles/read/713/converting-bytes-to-human-readable-values-kb-mb-gb-tb-pb-eb-zb-yb-with-javascript

    :param size: size in bytes
    """

    i = math.floor(math.log(size, 2) / math.log(1024, 2))
    units = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]

    return "{}{}".format(round(size / math.pow(1024, i), 2), units[i])
