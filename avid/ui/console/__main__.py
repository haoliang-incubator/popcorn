# pylint: disable=import-outside-toplevel

"""
prerequisites:
* ffprobe (by ffmpeg)
"""

import argparse
import logging

import trio
from rich.logging import RichHandler


async def main(args):
    from avid.ui.console import facts, scanning, querying

    await facts.fulfill()

    if args.op == "scan":
        await scanning.main(args.rebuild)
    elif args.op == "query":
        querying.main()
    else:
        raise SystemExit("unknown op")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", action="store_true")
    subparser = parser.add_subparsers(dest="op", required=True)

    scan = subparser.add_parser("scan")
    scan.add_argument("--rebuild", action="store_true")

    subparser.add_parser("query")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    logging.basicConfig(
        level="WARNING" if args.quiet else "DEBUG",
        style="{",
        datefmt="%H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )

    trio.run(main, args)
