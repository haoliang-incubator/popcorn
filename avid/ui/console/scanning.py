import hashlib
import json
import logging
from subprocess import CalledProcessError, CompletedProcess

import trio

from . import facts
from .entities import Video, VideoDB

_log = logging.getLogger(__name__)


async def _md5sum(file: trio.Path) -> str:
    md5 = hashlib.md5(b"")

    async with (await file.open("rb")) as fp:
        await fp.seek(50 << 20)
        onebite = 1 << 20
        total = 5 << 20
        for _ in range(total // onebite):
            chunk = await fp.read(onebite)  # type: ignore
            if chunk == b"":
                break
            md5.update(chunk)

    return md5.hexdigest()


async def _mimetype(file: trio.Path) -> str:
    command = ["file", "--mime-type", "--brief", str(file)]

    cp: CompletedProcess = await trio.run_process(
        command, capture_stdout=True, check=True
    )

    return cp.stdout.strip().decode()


async def _mediainfo(file: trio.Path):
    """
    :raise:
        * subprocess.CalledProcessError
        * json.JSONDecodeError
    """

    command = [
        "ffprobe",
        "-hide_banner",
        "-loglevel",
        "fatal",
        "-show_error",
        "-show_format",
        "-show_streams",
        "-show_programs",
        "-show_chapters",
        "-show_private_data",
        "-print_format",
        "json",
        str(file),
    ]

    cp: CompletedProcess = await trio.run_process(
        command, capture_stdout=True, check=True
    )

    return json.loads(cp.stdout)


async def _find_files(dir: trio.Path):
    # hardcoded
    allowed_suffixes = {".mp4", ".flv", ".3gp"}

    for file in await dir.iterdir():
        if await file.is_file():
            if file.suffix in allowed_suffixes:
                yield file
            else:
                _log.info("ignored %s, due to suffix", file)
            continue

        if await file.is_dir():
            async for _file in _find_files(file):
                yield _file
            continue


async def assemble_video(file: trio.Path):
    md5sum = await _md5sum(file)
    mimetype = await _mimetype(file)
    mediainfo = await _mediainfo(file)
    stat = await file.stat()

    def extract_mediainfo():
        def _duration(total: int):
            rest = int(total)
            hours = rest // 3600
            rest -= 3600 * hours
            minutes = rest // 60
            rest -= 60 * minutes
            seconds = rest

            return "{:02}:{:02}:{:02}".format(hours, minutes, seconds)

        tolerant_exc = (AssertionError, TypeError, KeyError, ValueError)
        try:
            duration_seconds = int(float(mediainfo["format"]["duration"]))
        except (TypeError, KeyError, ValueError):
            _log.exception("failed to extract duration")
            duration_seconds = 0

        yield _duration(duration_seconds)

        try:
            for stream in mediainfo["streams"]:
                if stream["codec_type"] == "video":
                    video_stream = stream
                    break
            else:
                raise ValueError("no video stream found")

            width = video_stream["coded_width"]
            height = video_stream["coded_height"]
        except tolerant_exc:
            _log.exception("failed to extract resolution")
            width = 0
            height = 0

        yield "{}x{}".format(width, height)

    extractor = extract_mediainfo()
    duration = next(extractor)
    resolution = next(extractor)

    video = Video(
        mime_type=mimetype,
        fingerprint=md5sum,
        location=str(file),
        born_at=stat.st_mtime,
        size=stat.st_size,
        resolution=resolution,
        duration=duration,
    )

    return video


async def producer(outbox: trio.MemorySendChannel):
    async with outbox:
        for lib in facts.Filesystem.libraries:
            async for file in _find_files(lib):
                await outbox.send(file)


async def consumer(inbox: trio.MemoryReceiveChannel, keep):
    async with inbox:
        async for file in inbox:
            try:
                video = await assemble_video(file)
            except CalledProcessError as e:
                _log.debug("failed: %s; reason: %s", file, e)
            else:
                keep(video)


async def main(rebuild: bool):

    videodb = VideoDB.from_file(facts.Filesystem.videodb)

    if videodb:
        if not rebuild:
            _log.info("stats: %s", videodb.stats())
            return
        videodb = VideoDB([])

    nursery: trio.Nursery
    async with trio.open_nursery() as nursery:
        outbox: trio.MemorySendChannel
        inbox: trio.MemoryReceiveChannel
        outbox, inbox = trio.open_memory_channel(max_buffer_size=100)

        async with outbox, inbox:
            nursery.start_soon(producer, outbox.clone())

            for _ in range(20):
                # 50 * 1M # peak random read
                nursery.start_soon(consumer, inbox.clone(), videodb.append)

    videodb.to_file(facts.Filesystem.videodb)
    _log.info("saved into %s; stats: %s", facts.Filesystem.videodb, videodb.stats())
