import logging
from typing import Union

import trio
from rich.logging import RichHandler
from wsproto.events import BytesMessage, TextMessage

from avid.websocketlib import (Logger, Message, Protocol, RequestProcessor,
                               StreamHandler)


def processor_factory(protocol: Protocol, logger: Logger):
    return EchoProcessor(protocol, logger)


class EchoProcessor(RequestProcessor):
    async def process(self, msg: Union[bytes, str]):
        resp: Message
        if isinstance(msg, bytes):
            resp = BytesMessage(data=msg)
        elif isinstance(msg, str):
            resp = TextMessage(data=msg)
        else:
            raise RuntimeError("unexpected msg type")

        await self.protocol.send_response(resp)


async def main():
    logging.info("serving websocket on %s", ":8085")
    await trio.serve_tcp(StreamHandler(processor_factory), 8085)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%H:%M:%S",
        format="{message}",
        handlers=[RichHandler()],
    )
    trio.run(main)
