from typing import List, Optional, Union

import attr
from wsproto.events import BytesMessage, TextMessage
from wsproto.utilities import LocalProtocolError, RemoteProtocolError

MessageContainer = Union["BinaryMessageContainer", "TextMessageContainer"]
Message = Union[TextMessage, BytesMessage]


class MessageCollector:
    """
    NB:
    * collecting, overflowed for collector
    * size, completed for container
    """

    # TODO@haoliang needs a FIFO queue?

    def __init__(self, capacity: int):
        self._container: Optional[MessageContainer] = None
        self._capacity = capacity

    def takeout(self):
        assert self._container is not None

        msg = self._container
        self._container = None

        return msg

    def _start_collecting(self, msgcls):
        if isinstance(self._container, msgcls):
            return

        if self._container is not None:
            raise RemoteProtocolError("inconsistent message type")

        msg = msgcls(fragments=[])
        self._container = msg

    def append_text(self, msg: TextMessage):
        """
        :raise: LocalProtocolError, RemoteProtocolError
        """

        self._start_collecting(TextMessageContainer)

        self._container.append(msg)  # type: ignore

    def append_binary(self, msg: BytesMessage):
        """
        :raise: LocalProtocolError
        """

        self._start_collecting(BinaryMessageContainer)

        self._container.append(msg)  # type: ignore

    @property
    def collecting(self) -> bool:
        return self._container is not None

    def _size(self) -> int:
        return self._container.size  # type: ignore

    @property
    def overflowed(self) -> bool:
        return self._size() > self._capacity

    @property
    def completed(self) -> bool:
        return self._container.completed  # type: ignore

    def __repr__(self):
        if self.collecting:
            return str(
                {
                    "container": type(self._container),
                    "completed": self.completed,
                    "size": self._size(),
                    "capacity": self._capacity,
                    "overflowed": self.overflowed,
                    "collecting": self.collecting,
                }
            )

        return str(
            {
                "container": "N/A",
                "completed": "N/A",
                "size": "N/A",
                "capacity": self._capacity,
                "overflowed": "N/A",
                "collecting": False,
            }
        )


@attr.s
class BinaryMessageContainer:
    content_type = BytesMessage
    # TODO@haoliang avoid unnecessary copy
    fragments: List[BytesMessage] = attr.ib()
    completed: bool = attr.ib(init=False, default=False)
    size: int = attr.ib(init=False, default=0)

    def append(self, msg: BytesMessage):
        assert isinstance(msg, self.content_type)

        if self.completed:
            raise LocalProtocolError("appending frame payload to completed container")

        self.fragments.append(msg)
        self.size += len(msg.data)
        self.completed = msg.message_finished

    def message(self) -> bytes:
        if not self.completed:
            raise LocalProtocolError("reading incomplete message!")

        return b"".join(frag.data for frag in self.fragments)


@attr.s
class TextMessageContainer:
    content_type = TextMessage
    # TODO@haoliang avoid unnecessary copy
    fragments: List[TextMessage] = attr.ib()
    completed: bool = attr.ib(init=False, default=False)
    size: int = attr.ib(init=False, default=0)

    def append(self, msg: TextMessage):
        assert isinstance(msg, self.content_type)

        if self.completed:
            raise LocalProtocolError("appending frame payload to completed container")

        self.fragments.append(msg)
        self.size += len(msg.data)
        self.completed = msg.message_finished

    def message(self) -> str:
        if not self.completed:
            raise LocalProtocolError("reading incomplete message!")

        return "".join(frag.data for frag in self.fragments)
