import logging
import traceback
from itertools import count
from typing import Union

import trio
from wsproto import ConnectionType, WSConnection
from wsproto.events import (
    AcceptConnection,
    BytesMessage,
    CloseConnection,
    Event,
    Ping,
    Request,
    TextMessage,
)

from .messages import MessageCollector


class Logger:
    def __init__(self, id):
        self._id = id
        self._log = logging.getLogger(__name__)

    def debug(self, msg, *args):
        self._log.debug(f"#%d {msg}", self._id, *args)

    def warning(self, msg, *args):
        self._log.warning(f"#%d {msg}", self._id, *args)

    def exception(self, exc: Exception, recent_first=False):
        def _frames():
            tbe = traceback.TracebackException.from_exception(
                exc, lookup_lines=True, capture_locals=True
            )

            frames = list(tbe.format())

            if recent_first:
                frames.reverse()

            return frames

        for frame in _frames():
            self._log.error(frame)


class Protocol:
    read_timeout = 10 * 60
    max_frame_size = 4 << 10
    max_size = 2 << 20

    def __init__(self, stream: trio.SocketStream, logger: Logger):
        self._stream = stream
        self._logger = logger
        self._ws = WSConnection(ConnectionType.SERVER)

    async def send_response(self, resp: Event):
        translated = self._ws.send(resp)
        self._logger.debug("sending response with %d bytes", len(translated))
        await self._stream.send_all(translated)

    async def read_from_peer(self):
        with trio.fail_after(self.read_timeout):
            outcome = await self._stream.receive_some(self.max_frame_size)

        if outcome == b"":
            raise trio.BrokenResourceError()

        self._ws.receive_data(outcome)

        return outcome

    def next_event(self):
        for event in self._ws.events():
            yield event

    async def shutdown(self):
        self._logger.debug("shutting down connection")

        timeout = 5

        try:
            await self._stream.send_eof()
        except trio.BrokenResourceError:
            return

        with trio.move_on_after(timeout):
            await self._stream.aclose()

    async def force_shutdown(self):
        self._logger.debug("force shutting down connection")

        await trio.aclose_forcefully(self._stream)


class StreamHandler:
    idgen = count()

    max_message_size = 2 << 20

    def __init__(self, processor_factory):
        self._processor_factory = processor_factory

    def _context(self, stream: trio.SocketStream):
        logger = Logger(next(StreamHandler.idgen))
        protocol = Protocol(stream, logger)
        collector = MessageCollector(self.max_message_size)
        processor: RequestProcessor = self._processor_factory(protocol, logger)

        return logger, protocol, collector, processor

    async def handle(self, stream: trio.SocketStream):
        logger, protocol, collector, processor = self._context(stream)

        while True:
            logger.debug("main loop: waiting request")

            try:
                await protocol.read_from_peer()
            except trio.TooSlowError:
                await protocol.send_response(
                    CloseConnection(code=1001, reason="read timeout")
                )
                await protocol.shutdown()
                break
            except trio.BrokenResourceError:
                await protocol.force_shutdown()
                break

            for event in protocol.next_event():

                if collector.collecting and collector.overflowed:
                    await protocol.send_response(
                        CloseConnection(code=1009, reason="message too big")
                    )
                    await protocol.shutdown()
                    return

                if isinstance(event, Request):
                    logger.debug("accepting request: %s", event)
                    await protocol.send_response(AcceptConnection())
                    continue

                if isinstance(event, CloseConnection):
                    logger.debug("closing connection: %s", event)
                    await protocol.send_response(event.response())
                    await protocol.shutdown()
                    return

                if isinstance(event, Ping):
                    logger.debug("recv ping: %s", event)
                    await protocol.send_response(event.response())
                    continue

                if isinstance(event, TextMessage):
                    logger.debug("recv text: len=%d", len(event.data))
                    collector.append_text(event)
                    logger.debug("collector: %s", collector)
                    if collector.completed:
                        # TODO@haoliang timeout
                        with trio.fail_after(10):
                            await processor.process(collector.takeout().message())
                elif isinstance(event, BytesMessage):
                    logger.debug("recv bytes: len=%d", len(event.data))
                    collector.append_binary(event)
                    logger.debug("collector: %s", collector)
                    if collector.completed:
                        # TODO@haoliang timeout
                        with trio.fail_after(10):
                            await processor.process(collector.takeout().message())
                else:
                    raise RuntimeError("unknown event: {}".format(event))

    __call__ = handle


class RequestProcessor:
    def __init__(self, protocol: Protocol, logger: Logger):
        self.protocol = protocol
        self.logger = logger

    async def process(self, msg: Union[bytes, str]):
        """
        things need to do:
        * processing incomming msg
        * responsing via protocol.send_response
        """
        raise NotImplementedError()
