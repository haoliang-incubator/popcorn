
backlog
---

* enough checkpoint?
* hot loop: Obj(), func()

content-negotiation
* request.headers.accept
* endpoint.{json,text,html}

potential ddos:
* ref [hypercorn](https://pgjones.gitlab.io/hypercorn/discussion/dos_mitigations.html)
* timing: client reads response slowly

cases can block event loop:
* blocking operation
    * disk io: open
    * network io: requests.get
* cpu-bound operations
    * fibonacci

rate limit
* based on: ip
* via nginx?

tests
---

* keep-alive, connection-close
* in-process socket server
    * https://trio.readthedocs.io/en/stable/reference-testing.html#connecting-to-an-in-process-socket-server
* curl -F 'file-x=@/path/to/path' # multipart/form-data
* curl -F 'name-x=value' # multipart/form-data
* curl -d 'name-x=value' # application/x-www-form-urlencoded
* incorrect content-length in request message

* websocket text frame income, out text frame; same as binary
* websocket multiple frames for complete message
* websocket termination on client side without sending close frame

how-to
---

### deal with headers:

1) see `h11._headers.__doc__`
2) source: h11.{request,response}.headers
3) uses lower header name
4) type: List[Tuple[bytes, bytes]]

before construction
* constructor.\_parsed = False
* header pair: (Union[str, bytes], Union[str, bytes])

after construction
* h11.{request/response}.headers.{append,insert,extend}
* header pair: (bytes, bytes)
