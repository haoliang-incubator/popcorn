FROM python:3.8-slim

# speed up pip
RUN mkdir /root/.pip
COPY pip.conf /root/.pip/pip.conf

# install project dependencies
COPY requirements.txt /avid.requirements.txt
RUN pip install -r /avid.requirements.txt

ARG THEMAN="haoliang"
ENV THEMAN=$THEMAN

RUN useradd -u 1000 -g 100 --create-home $THEMAN

ENV SHELL=/bin/bash \
    # locale
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8

# locale
RUN ln -sf /usr/share/zoneinfo/PRC /etc/localtime

# run as unprivileged user
USER $THEMAN
