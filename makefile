.PHONY: install tests

freeze_as_requirements:
	@ poetry export --format requirements.txt --without-hashes | tee requirements.txt > docker/requirements.txt

install: freeze_as_requirements
	@ python3 -m venv venv
	@ venv/bin/pip install -r requirements.txt

serve.web: install
	@ venv/bin/python -m avid.ui.web

serve.web.benchmark: install
	@ venv/bin/python -m avid.ui.web --quiet

serve.web.tests: install
	@ venv/bin/python -m avid.ui.webtests


serve.websocket:
	@ venv/bin/python -m avid.ui.websocket

lint:
	@ poetry run mypy --show-column-numbers --ignore-missing-imports avid
	@ poetry run pylint avid

tests:
	@ poetry run pytest --color=yes --show-capture=all

serve.couchdb:
	@ docker-compose up --build -d couchdb

shell:
	@ PYTHONPATH=${PWD} poetry shell
